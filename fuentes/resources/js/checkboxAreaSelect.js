/*****************************************
 * Checkbox Area Select:  version 1.0 Created by Harry Pottash
 * Contact hpottash@gmail.com for further information
 * http://www.7goldfish.com
 *
 *  To apply to your page include this file and
 *  add the js call: $(document).checkboxAreaSelect()
 *
 *  Example:
 *    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js'></script>
 *    <script type='text/javascript' src='<path to your js>/checkboxAreaSelect.js'></script>
 *    <script type='text/javascript'>
 *      $(document).ready(function(){
 *        $(document).checkboxAreaSelect();
 *      });
 *    </script>
 *
 *
 *
 *  To use drag the mouse over checkboxes to click them
 *  Shift+Drag the mouse over checkboxes to unclick them
 *
 * Released under GPL version 3
 *
 ****************************************/
$.fn.checkboxAreaSelect = function() {
    var cbAS_startX;
    /* where the click started, X & Y */
    var cbAS_startY;
    var cbAS_mouseIsDown;
    var cbAS_colorAdd = "green";
    var cbAS_colorRemove = "red";

    /*flag to record if mouse is currently draging */

    /*when a mouse clicks down, prepair to start a drag*/
    $(document).mousedown(function(e) {

        cbAS_startX = e.pageX;
        cbAS_startY = e.pageY;
        /*record where the mouse started */
        $("body").append("<div id='cbAS_dragbox'></div>");
        /*create a graphic indicator of select area */
        $("#cbAS_dragbox").css({
            //"background-color":"red",
            //opacity:".20",
            "border":"1px solid " + cbAS_colorAdd,
            position:"absolute",
            left: cbAS_startX + "px",
            top: cbAS_startY + "px",
            width: "0px",
            height: "0px"});
        cbAS_mouseIsDown = true;
        /*flag that the mouse is down */
    });
    /*close mousedown*/


    /*if the mouse is moving run this*/
    $(document).mousemove(function(e) {
        if (cbAS_mouseIsDown) { /*check if they are currently dragging the mouse*/
            dragHeight = e.pageY - cbAS_startY;
            dragWidth = e.pageX - cbAS_startX;
            /*find the x & y diff of where they are and where they started */

            if (e.shiftKey) {
                $("#cbAS_dragbox").css({"border":"1px solid " + cbAS_colorRemove});
            } else {
                $("#cbAS_dragbox").css({"border":"1px solid " + cbAS_colorAdd});
            }

            $("#cbAS_dragbox").css({ height: -dragHeight ,  width: -dragWidth, left: e.pageX, top: e.pageY});
            /*make the colored box fit the mouse movements */
            if (dragHeight < 0 && dragWidth < 0) { /* up and to the left */
                $("#cbAS_dragbox").css({ height: -dragHeight ,  width: -dragWidth, left: e.pageX, top: e.pageY});
            } else if (dragHeight < 0 && dragWidth > 0) { /*up and to the right */
                $("#cbAS_dragbox").css({ height: -dragHeight ,  width: dragWidth, left: cbAS_startX, top: e.pageY});
            } else if (dragHeight > 0 && dragWidth < 0) { /* down and to the left */
                $("#cbAS_dragbox").css({ height: dragHeight ,  width: -dragWidth, left: e.pageX, top: cbAS_startY});
            } else { /* down and to the right */
                $("#cbAS_dragbox").css({ height: dragHeight , width: dragWidth, left: cbAS_startX, top: cbAS_startY});
            }
        }
    });

    /* when they release the mouse button, check if they have dragged over any checkboxes,
     If they have, do work on them. Also reset things that started on mouse-down */
    $(document).mouseup(function(e) {
        cbAS_mouseIsDown = false;
        /*cleare currently dragging flag */
        $("#cbAS_dragbox").remove();
        /*get rid of select box */
        endX = e.pageX;
        endY = e.pageY;
        /*discover where mouse was released x&y */

        /*for each checkbox on the page check if its within the drag-area*/
        $(":checkbox").each(function() {
            box_top = $(this).position().top + ($(this).height() / 2);
            /*checkboxes have an area */
            box_left = $(this).position().left + ($(this).width() / 2);
            /*so find their centerpoint */
            if ((box_top > cbAS_startY && box_top < endY ) || (box_top < cbAS_startY && box_top > endY )) {
                if ((box_left > cbAS_startX && box_left < endX ) || (box_left < cbAS_startX && box_left > endX )) {
                    /*if checkbox was in the drag area */
                    if (e.shiftKey) {
                        $(this).attr("checked", false);
                        /*uncheck due to shift key */
                    } else {
                        $(this).attr("checked", true);
                        /*check the box */
                    }
                }
            }

        });
        /*close each*/
    });
    /*close mouseup*/
};
/*close checkboxAreaSelect*/
