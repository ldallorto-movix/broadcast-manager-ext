<%@ page import="com.movix.shared.servlet.BaseServlet" %>
<%@ page import="com.movix.shared.util.NewJDynamiTe" %>
<%@ page import="com.movixla.broadcast.shared.Broadcast" %>
<%@ page import="com.movixla.broadcastmanager.aplicacion.domobj.Mensaje" %>
<%@ page import="java.util.Map" %>
<%
    NewJDynamiTe jDynamiTe = (NewJDynamiTe) request.getAttribute(BaseServlet.TEMPLATE_OBJ_PATH_KEY);
    Map varValues = (Map) request.getAttribute(BaseServlet.TEMPLATE_VARS_VALUES);


    Broadcast broadcast = (Broadcast) varValues.get("broadcast");
    Mensaje mensaje = (Mensaje) varValues.get("mensaje");
    String pruebas = (String) varValues.get("pruebas");
    String error = (String) varValues.get("error");
    Boolean inicio = (Boolean) varValues.get("inicio");
    String bcm = (String) varValues.get("bcm");

    jDynamiTe.setVariable("BROADCAST_ID", broadcast.getId());
    jDynamiTe.setVariable("BROADCAST_NOMBRE", broadcast.getNombre());
    jDynamiTe.setVariable("OPERADOR", broadcast.getOperador());
    jDynamiTe.setVariable("LA", mensaje.getLa());
    jDynamiTe.setVariable("TIPO", mensaje.getTipo());
    jDynamiTe.setVariable("TEXTO", mensaje.getTexto());
    jDynamiTe.setVariable("URL", mensaje.getUrl());
    jDynamiTe.setVariable("PRUEBAS", pruebas);
    jDynamiTe.setVariable("BCM", bcm);

    if (!inicio) {
        if (error != null && error.trim().length() > 0) {
            jDynamiTe.setVariable("ERROR", error);
        } else {
            jDynamiTe.setVariable("ERROR", "Prueba Enviada. En caso de no recibir la prueba comuniquese con el area de desarrollo");
        }
    }

%>
