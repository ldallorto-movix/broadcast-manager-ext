<%@ page import="com.movix.shared.servlet.BaseServlet" %>
<%@ page import="com.movix.shared.util.NewJDynamiTe" %>
<%@ page import="com.movixla.broadcast.shared.Broadcast" %>
<%@ page import="com.movixla.broadcastmanager.aplicacion.domobj.Perfil" %>
<%@ page import="com.movixla.broadcastmanager.aplicacion.domobj.Usuario" %>
<%@ page import="com.movixla.broadcastmanager.aplicacion.WebUtil" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Map" %>
<%
    NewJDynamiTe jDynamiTe = (NewJDynamiTe) request.getAttribute(BaseServlet.TEMPLATE_OBJ_PATH_KEY);
    Map varValues = (Map) request.getAttribute(BaseServlet.TEMPLATE_VARS_VALUES);

    SimpleDateFormat f_yyyyMMdd_HHmm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    jDynamiTe.setVariable("HORA_UTC", f_yyyyMMdd_HHmm.format(new Date()));


    Map<String, Broadcast> broadcasts = (Map<String, Broadcast>) varValues.get("broadcasts");
    String filtrar = (String) varValues.get("filtrar");
    boolean incluirCancelados = (Boolean) varValues.get("filtrar_cancelado");
    boolean incluirTerminados = (Boolean) varValues.get("filtrar_terminado");
    
    jDynamiTe.setVariable("LOGO", "headerBCM.jpg");
    jDynamiTe.setVariable("FILTRAR", filtrar);
    jDynamiTe.setVariable("FILTRAR_CANCELADO", incluirCancelados ? "checked" : "");
    jDynamiTe.setVariable("FILTRAR_TERMINADO", incluirTerminados ? "checked" : "");

    Usuario usuario = (Usuario) varValues.get("usuario");
    Perfil perfil = (Perfil) varValues.get("perfil");

    jDynamiTe.parseDynElem("cuadroBusqueda");
    jDynamiTe.parseDynElem("bcManagerFast");
    
     //PARAMETRIZACION SEGUN PERFIL
	jDynamiTe.setVariable("LOGO", perfil.getImagenLogo());
	
    
    Iterator it = broadcasts.entrySet().iterator();
    while (it.hasNext()) {
        Map.Entry broadcast = (Map.Entry) it.next();
        String bcId = (String) broadcast.getKey();
        Broadcast bc = (Broadcast) broadcast.getValue();

        if (bc.getEstado().equals(Broadcast.Estado.ELIMINADO)) {
            continue;
        }
        //no mostramos las repeticiones terminadas
        if (bc.getEstado().equals(Broadcast.Estado.TERMINADO) && bc.getNombre().startsWith("[R]")) {
            continue;
        }

        jDynamiTe.setVariable("ID", bc.getId());
        jDynamiTe.setVariable("NOMBRE", bc.getNombre());
        jDynamiTe.setVariable("OPERADOR", bc.getOperador());
        jDynamiTe.setVariable("FECHA_INICIO", f_yyyyMMdd_HHmm.format(bc.getFechaInicio()));
        jDynamiTe.setVariable("FECHA_FIN", f_yyyyMMdd_HHmm.format(bc.getFechaFin()));
        jDynamiTe.setVariable("ESTADO", bc.getEstado());

        jDynamiTe.setVariable("BROADCAST_ENVIOS_TOTAL", bc.getEnviosTotal());
        jDynamiTe.setVariable("BROADCAST_ENVIOS_OK", bc.getEnviosOk());
        jDynamiTe.setVariable("BROADCAST_ENVIOS_ERROR", bc.getEnviosError());
        jDynamiTe.setVariable("BROADCAST_ENVIOS_LISTANEGRA", bc.getEnviosListaNegra());
        jDynamiTe.setVariable("BROADCAST_ENVIOS_LNU", bc.getEnviosListaNegraUnificada());


        jDynamiTe.setVariable("ESTADO_DETALLE", bc.getEstadoDetalle() != null ? bc.getEstadoDetalle() : "");

        //solo si esta en proceso se puede detener.
        jDynamiTe.setVariableCond("DETENER_DISABLED", !bc.getEstado().equals(Broadcast.Estado.EN_PROCESO), "_off", "");
        //solo si esta detenido se puede reiniciar
        jDynamiTe.setVariableCond("REANUDAR_DISABLED", !bc.getEstado().equals(Broadcast.Estado.DETENIDO), "_off", "");

        jDynamiTe.clearDynElemValue("enable_reanudar");
        jDynamiTe.clearDynElemValue("enable_detener");
        jDynamiTe.parseDynElemCond(bc.getEstado().equals(Broadcast.Estado.DETENIDO), "enable_reanudar", "enable_detener");

        //si esta inactivo se no se puede editar o cancelar
        boolean inactivo = WebUtil.isInactivo(bc);
        //jDynamiTe.setVariableCond("EDITAR_DISABLED", inactivo, "disabled", "");
        jDynamiTe.setVariableCond("CANCELAR_DISABLED", inactivo, "_off", "");

        jDynamiTe.parseDynElem("broadcasts");
    }
%>
