<%@ page import="com.movix.shared.Operador" %>
<%@ page import="com.movix.shared.servlet.BaseServlet" %>
<%@ page import="com.movix.shared.util.NewJDynamiTe" %>
<%@ page import="com.movix.shared.util.Util" %>
<%@ page import="com.movixla.broadcastmanager.aplicacion.domobj.Perfil" %>
<%@ page import="com.movixla.broadcastmanager.aplicacion.domobj.Usuario" %>
<%@ page import="com.movixla.broadcastmanager.aplicacion.domobj.Mensaje" %>
<%@ page import="com.movixla.broadcast.shared.*" %>

<%@ page import="com.movixla.broadcastmanager.aplicacion.WebUtil" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%
    NewJDynamiTe jDynamiTe = (NewJDynamiTe) request.getAttribute(BaseServlet.TEMPLATE_OBJ_PATH_KEY);
    Map varValues = (Map) request.getAttribute(BaseServlet.TEMPLATE_VARS_VALUES);

    SimpleDateFormat f_yyyyMMdd_HHmm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    jDynamiTe.setVariable("HORA_UTC", f_yyyyMMdd_HHmm.format(new Date()));

    SimpleDateFormat f_yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat f_HHmm = new SimpleDateFormat("HH:mm");

    Broadcast broadcast = (Broadcast) varValues.get("broadcast");
    String bcm = (String) varValues.get("bcm");

    Mensaje mensaje = (Mensaje) varValues.get("mensaje");
	List<ElementoMensaje> elementosMensajes = (List<ElementoMensaje>) varValues.get("elementosMensajes");
    if (mensaje != null) {
        elementosMensajes.add(0, WebUtil.getElementoMensajeTextoDummy(mensaje));
        elementosMensajes.add(0, WebUtil.getElementoMensajeImagenDummy(mensaje));
    }

    List<Notificacion> notificaciones = (List<Notificacion>) varValues.get("notificaciones");
    notificaciones.add(0, WebUtil.getNotificacionDummy(broadcast));

    List<String> errores = (List<String>) varValues.get("errores");

    Usuario usuario = (Usuario) varValues.get("usuario");
	Perfil perfil = (Perfil) varValues.get("perfil");

    //ERRORES
    for (String error : errores) {
        jDynamiTe.setVariable("ERROR", error);
        jDynamiTe.parseDynElem("errores");
    }

    jDynamiTe.setVariable("BCM", bcm);

    // DATOS BASICOS DEL BROADCAST
    jDynamiTe.setVariable("ID", broadcast.getId());
    jDynamiTe.setVariable("NOMBRE", broadcast.getNombre());
    jDynamiTe.setVariable("DESCRIPCION", broadcast.getDescripcion());

    String fecha_ini_txt = broadcast.getFechaInicio() != null ? f_yyyyMMdd_HHmm.format(broadcast.getFechaInicio())
            : f_yyyyMMdd_HHmm.format(WebUtil.getFechaInicioDefault());
    String fecha_fin_txt = broadcast.getFechaFin() != null ? f_yyyyMMdd_HHmm.format(broadcast.getFechaFin())
            : f_yyyyMMdd_HHmm.format(WebUtil.getFechaFinDefault());

    fecha_ini_txt = fecha_ini_txt + " UTC";
    fecha_fin_txt = fecha_fin_txt + " UTC";
    jDynamiTe.setVariable("FECHA_INICIO", fecha_ini_txt);
    jDynamiTe.setVariable(
            "FECHA_INICIO_TZ",
            WebUtil.tranformaUTCaTimeZone(fecha_ini_txt, "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mm z",
                    broadcast.getOperador()));
    jDynamiTe.setVariable("FECHA_FIN", fecha_fin_txt);
    jDynamiTe.setVariable(
            "FECHA_FIN_TZ",
            WebUtil.tranformaUTCaTimeZone(fecha_fin_txt, "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mm z",
                    broadcast.getOperador()));

    jDynamiTe.setVariable("TASA", broadcast.getEnviosPorSegundo());
    
    //PARAMETRIZACION SEGUN PERFIL
	jDynamiTe.setVariable("TASA_DISABLED", perfil.getTasa_editable());
	jDynamiTe.setVariable("MENSAJE_LA_DISABLED", perfil.getMensaje_la_editable());
	jDynamiTe.setVariable("MENSAJE_SERVICIO_DISABLED", perfil.getMensaje_servicio_editable());
	jDynamiTe.setVariable("MENSAJE_SERVICIO_STYLE", perfil.getMensaje_servicio_visible());
	
	jDynamiTe.setVariable("MENSAJE_TITULO_DISABLED", perfil.getMensaje_texto_editable());
	jDynamiTe.setVariable("SERVICIO_REGA_DISABLED", perfil.getMensaje_sregala_editable());
	jDynamiTe.setVariable("MENSAJE_URL_DISABLED", perfil.getMensaje_url_editable());
	jDynamiTe.setVariable("SMS_FINAL_DISABLED", perfil.getMensaje_smsfinal_editable());
	
	
	jDynamiTe.setVariable("NOTIFICACIONES_VISIBLE", perfil.getNotificaciones_visible());
	jDynamiTe.setVariable("LISTA_NEGRA_VISIBLE", perfil.getListanegra_visible());
	jDynamiTe.setVariable("MENSAJE_SERVICIO_VISIBLE", perfil.getMensaje_servicio_visible());
	

    jDynamiTe.setVariable("BASE_ARCHIVO_FILE", Util.noNull(broadcast.getBasePathArchivo()));
    jDynamiTe.setVariableCond("BASE_ARCHIVO_CHECKED", broadcast.getBaseTipo().equals(Broadcast.BaseTipo.ARCHIVO),"checked", "");
  
    
    jDynamiTe.setVariable("LISTA_NEGRA_FILE", Util.noNull(broadcast.getListaNegraPathArchivo()));
    jDynamiTe.setVariableCond("LN_ARCHIVO_CHECKED", broadcast.getListaNegraTipo().equals(Broadcast.ListaNegraTipo.ARCHIVO), "checked", "");
    
    //OPERADOR
    Operador[] operadors = Operador.values();
    for (Operador operador : operadors) {
        if (perfil.getOperadores().contains(operador)) {
        	jDynamiTe.setVariableCond("OPERADOR_SELECTED", broadcast.getOperador() == operador, "selected", "");
        	jDynamiTe.setVariable("OPERADOR_NOMBRE", operador.name());
        	jDynamiTe.parseDynElem("operadores");
        }
    }
    
    //MENSAJES TIPO
    Mensaje.Tipo[] tiposm = Mensaje.Tipo.values();
    for (Mensaje.Tipo tipom : tiposm) {
        if (tipom == Mensaje.Tipo.SMS_SACA) continue;
        
        if (perfil.getMensaje_tipos().contains(tipom)) {
        	jDynamiTe.setVariableCond("MENSAJETIPO_SELECTED", mensaje.getTipo() == tipom, "selected", "");
        	jDynamiTe.setVariable("MENSAJETIPO_NOMBRE", tipom.name());
        	jDynamiTe.parseDynElem("mensajestipo");
        }
    }
    
    jDynamiTe.setVariable("MENSAJE_TITULO", mensaje.getTexto());
    jDynamiTe.setVariable("MENSAJE_SUBJECT", mensaje.getTexto());
    jDynamiTe.setVariable("MENSAJE_URL", mensaje.getUrl() != null ? mensaje.getUrl() : "");
    jDynamiTe.setVariable("MENSAJE_LA", mensaje.getLa());
    jDynamiTe.setVariable("MENSAJE_SERVICIO", mensaje.getServicio());
    jDynamiTe.setVariable("SERVICIO_REGA", mensaje.getServicioRegala());
    jDynamiTe.setVariable("MENSAJE_PRECIO", mensaje.getPrecio());
    jDynamiTe.setVariable("SMS_FINAL", mensaje.getSmsFinal());
    
    // ELEMENTOS MENSAJES
    for (ElementoMensaje elementoMensaje : elementosMensajes) {
        if (elementoMensaje.getId() > 0 && ElementoMensaje.Estado.ELIMINADO == elementoMensaje.getEstado()) {
            continue;
        }
        jDynamiTe.clearDynElemValue("elementoMensajeTexto");
        jDynamiTe.clearDynElemValue("elementoMensajeImagen");
        jDynamiTe.setVariable("ELEMENTO_MENSAJE_ID", elementoMensaje.getId());
        jDynamiTe.setVariable("ELEMENTO_MENSAJE_TIPO", elementoMensaje.getTipo());
        jDynamiTe.setVariable("ELEMENTO_MENSAJE_DURACION", elementoMensaje.getDuracion());
        if (ElementoMensaje.Tipo.TEXT == elementoMensaje.getTipo()) {
            jDynamiTe.setVariable("ELEMENTO_MENSAJE_TEXTO",
                    null == elementoMensaje.getTexto() ? "" : elementoMensaje.getTexto());
            jDynamiTe.parseDynElem("elementoMensajeTexto");
        }
        else if (ElementoMensaje.Tipo.IMG == elementoMensaje.getTipo()) {
            jDynamiTe.setVariable("ELEMENTO_MENSAJE_URL",
                    null == elementoMensaje.getUrl() ? "" : elementoMensaje.getUrl());
            jDynamiTe.parseDynElem("elementoMensajeImagen");
        }
        jDynamiTe.parseDynElem("elementosMensajes");
    }

    
    // NOTIFICACIONES
    for (Notificacion notificacion : notificaciones) {
        if (notificacion.getId() > 0 && !notificacion.getEstado().equals(Notificacion.Estado.GUARDADO)) {
            continue;
        }
        jDynamiTe.setVariable("NOTIFICACION_ID", notificacion.getId());
        jDynamiTe.setVariable("NOTIFICACION_DESTINATARIOS", notificacion.getDestinatarios());
        jDynamiTe.setVariable("NOTIFICACION_CADA", notificacion.getCada());
        
        jDynamiTe.parseDynElem("notificaciones");
    }

    jDynamiTe.parseDynElemCond(broadcast.getEstado().equals(Broadcast.Estado.NUEVO), "guardar_nuevo",
            "guardar_guardado");

    if (WebUtil.isInactivo(broadcast)) {
        jDynamiTe.parseDynElem("javascript_inactivo");
    }
    else if (WebUtil.isEnProceso(broadcast)) {
        jDynamiTe.parseDynElem("javascript_en_proceso");
    }
%>
