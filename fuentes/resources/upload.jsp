<%@ page import="com.movix.shared.servlet.BaseServlet" %>
<%@ page import="com.movix.shared.util.NewJDynamiTe" %>
<%@ page import="java.util.Map" %>
<%
    NewJDynamiTe jDynamiTe = (NewJDynamiTe) request.getAttribute(BaseServlet.TEMPLATE_OBJ_PATH_KEY);
    Map varValues = (Map) request.getAttribute(BaseServlet.TEMPLATE_VARS_VALUES);


    String tipo = (String) varValues.get("tipo");
    int broadcastId = (Integer) varValues.get("broadcastId");
    int elementoId = (Integer) varValues.get("elementoId");
    String bcm = (String) varValues.get("bcm");

    jDynamiTe.setVariable("TIPO", tipo);
    jDynamiTe.setVariable("BROADCAST_ID", broadcastId);
    jDynamiTe.setVariable("ELEMENTO_ID", elementoId);
    jDynamiTe.setVariable("BCM", bcm);


%>
