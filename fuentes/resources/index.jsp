<%@ page import="com.movix.shared.servlet.BaseServlet" %>
<%@ page import="com.movix.shared.util.NewJDynamiTe" %>
<%@ page import="java.util.Map" %>
<%
    NewJDynamiTe jDynamiTe = (NewJDynamiTe) request.getAttribute(BaseServlet.TEMPLATE_OBJ_PATH_KEY);
    Map varValues = (Map) request.getAttribute(BaseServlet.TEMPLATE_VARS_VALUES);

    Boolean error = (Boolean) varValues.get("error");

    if (error) {
        jDynamiTe.parseDynElem("error");
    }
%>
