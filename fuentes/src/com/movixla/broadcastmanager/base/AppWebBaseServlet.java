package com.movixla.broadcastmanager.base;

import com.movix.shared.servlet.BaseServlet;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Andres Cheuque
 *         Date: 29-ene-2008
 *         Time: 10:30:37
 */
public class AppWebBaseServlet extends BaseServlet {
    protected static final Logger logger = Logger.getLogger(AppWebBaseServlet.class);

    /*
     * Procesa peticiones http
     */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("ISO-8859-1");
            super.service(request, response);
        }
        catch (Throwable e) {
            logger.error("ERROR INESPERADO : " + e.getMessage(), e);
        }

    }

    /*
     * Procesa parseo de pagina.
     */
    protected String processParsedPage(String page) {
        String s = page.replaceAll("selected=\"\"", "");
        s = s.replaceAll("checked=\"\"", "");
        s = s.replaceAll("disabled=\"\"", "");
        return s;
    }
}
