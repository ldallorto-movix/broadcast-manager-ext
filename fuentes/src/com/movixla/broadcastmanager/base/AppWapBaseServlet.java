package com.movixla.broadcastmanager.base;

import com.movix.shared.servlet.WapServlet;
import com.movix.shared.telefono.InfoTelefono;
import com.movix.shared.telefono.InfoTelefonoManager;
import com.movix.shared.util.Util;
import com.movix.shared.wap.WapUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @author Andres Cheuque
 *         Date: 29-ene-2008
 *         Time: 10:22:31
 */
public class AppWapBaseServlet extends WapServlet {
    protected static final Logger logger = Logger.getLogger(AppWapBaseServlet.class);

    /*
     * Procesamiento de peticiones http
     */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("ISO-8859-1");
            response.setContentType("text/vnd.wap.wml");

            super.service(request, response);
        }
        catch (Throwable e) {
            logger.error("ERROR INESPERADO, \nparams: " + Util.parameters2String(request) + "\nheaders: " + Util.headers2String(request) + "page:" + request.getRequestURI(), e);
        }
    }

    /*
     * Show template http
     */
    protected void showTemplate(HttpServletRequest request, HttpServletResponse response, Map map) throws ServletException, IOException {
        //String agent = WapUtil.getUserAgentFull(request);
        InfoTelefono telefono = getTelefonoDevice(request);

        if (telefono.getWap().equals(InfoTelefono.Wap.W2_0)) {
            String tplPath = getTemplate(request, "xhtml");
            if (checkFileExists(tplPath)) {
                //ojo ponemos el content type de inmediato para poder consultarlo luego en los includes y asi sabersi incluir xhtml o wml
                response.setContentType("application/vnd.wap.xhtml+xml");
                showExternalTemplate(tplPath, request, response, map, "application/vnd.wap.xhtml+xml");
                return;
            }

        }

        String tplPath = getTemplate(request, "twml");
        response.setContentType("text/vnd.wap.wml");
        showExternalTemplate(tplPath, request, response, map, "text/vnd.wap.wml");
    }

    private static InfoTelefono getTelefonoDevice(HttpServletRequest request) {
        String ua = WapUtil.getUserAgentFull(request);

        if (ua == null || ua.trim().equals("")) ua = "no_ua";

        InfoTelefono telefono = InfoTelefonoManager.getInstance().getPorUserAgent(ua);
        if (telefono == null) {
            return new InfoTelefono(ua + "[EMULADO]", false, "", "", "", InfoTelefono.Wap.W1_0, null);
        }
        return telefono;
    }

    public String getTemplate(HttpServletRequest request, String extension) {
        String servletPath = request.getServletPath();
        String tplPath = servletPath.substring(0, servletPath.length() - SERVLET_EXT.length()) + "." + extension;
        return tplPath;
    }

    public boolean checkFileExists(String tpl) {
        return new File(servletContext.getRealPath(tpl)).exists();
    }
}