package com.movixla.broadcastmanager.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.movix.shared.Operador;
import com.movix.shared.util.RequestUtil;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.WebUtil;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.UsuariosManagerWrapper;
import com.movixla.broadcastmanager.base.AppWebBaseServlet;

/**
 * @author Andres Cheuque
 *         Date: 29-ene-2008
 *         Time: 10:37:15
 */
public class IndexServlet extends AppWebBaseServlet {

    private static final long serialVersionUID = 1L;

    // private static Properties properties = null;
    //
    // static{
    // properties = Configuration.forMe(IndexServlet.class);
    // }

    /*
     * doGet del servicio http
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<String, Object> varValues = new HashMap<String, Object>();

        HttpSession session = request.getSession(true);
        session.setAttribute("usuario", null);

        varValues.put("error", Boolean.FALSE);
        logger.info("Visualiza Index");
        showTemplate(request, response, varValues);
    }

    /*
     * doPost del servicio http
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TIENE QUE SER FALSE PARA UN PASO A PRODUCCION
        boolean pruebaDesarrollo = false;

        HashMap<String, Object> varValues = new HashMap<String, Object>();

        String user = RequestUtil.getParameter(request, "user", "");
        String pass = RequestUtil.getParameter(request, "pass", "");

        logger.info("Usuario=" + user);
        logger.info("Password=" + pass);

        logger.info("Usuario valido para incidencia =" + user);
        Usuario usuario = null;

        if (pruebaDesarrollo) {
             ArrayList<Operador> operadores = new ArrayList<Operador>();
             operadores.add(Operador.ENTEL);
             usuario = new Usuario(444, "test", "test", Usuario.Tipo.ADMIN,0);
        }
        else {
            try {
                usuario = UsuariosManagerWrapper.getUsuarioByUsernamePassword(user, pass);
            }
            catch (SQLException e) {
                logger.error("Error al recuperar usuario: " + user, e);
            }
        }

        if (usuario != null) {
            WebUtil.setUsuarioSesion(request, usuario);
            response.sendRedirect("lista.jhtml");
            logger.info("Exito en la validacion");
            return;
        }
        else {
            logger.info("Error ");
            varValues.put("error", Boolean.TRUE);
            showTemplate(request, response, varValues);
        }
    }

    // if (user.equals("jmaya") || user.equals("asam") || user.equals("cgiraldo") || user.equals("dsuarez") &&
    // pass.equals("incidencialdapfail") ){
    //
    // logger.info("Usuario valido para incidencia =" + user);
    // Usuario usuario = null;
    // try {
    // //[asam] Incidencia 20120429
    // //usuario = UsuariosManagerWrapper.getUsuarioByUsernamePassword(user, pass);
    // usuario = UsuariosManagerWrapper.getUsuarioByUsername(user);
    // } catch (SQLException e) {
    // logger.error("Error al recuperar usuario: " + user, e);
    // }
    //
    // if (usuario != null) {
    // WebUtil.setUsuarioSesion(request, usuario);
    // response.sendRedirect("lista.jhtml");
    // logger.info("Exito en la validacion");
    // return;
    // } else {
    // logger.info("Error ");
    // varValues.put("error", Boolean.TRUE);
    // showTemplate(request, response, varValues);
    // }
    // }else {
    // logger.info("El usuario no esta registrado para esta incidencia | No se ingresa a BCM : " +user);
    // }

}
