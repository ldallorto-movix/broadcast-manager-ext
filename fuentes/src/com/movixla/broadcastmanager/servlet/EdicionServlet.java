package com.movixla.broadcastmanager.servlet;

import com.movix.shared.Operador;
import com.movix.shared.util.RequestUtil;
import com.movix.shared.util.Util;
import com.movix.shared.wap.WapUtil;
import com.movixla.broadcastmanager.aplicacion.ClienteBroadcastManager;
import com.movixla.broadcastmanager.aplicacion.WebUtil;
import com.movixla.broadcastmanager.aplicacion.bo.BCMConexionBO;
import com.movixla.broadcastmanager.aplicacion.domobj.BCMConexionDO;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;
import com.movixla.broadcastmanager.aplicacion.domobj.Perfil;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.*;
import com.movixla.broadcastmanager.base.AppWebBaseServlet;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.Conexion;
import com.movixla.broadcast.shared.ElementoMensaje;
import com.movixla.broadcast.shared.Notificacion;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Andres
 * Date: 18-may-2009
 * Time: 16:59:36
 */
public class EdicionServlet extends AppWebBaseServlet {

    
    static{

    }

    /**
     * Rescata valores para el despliegue de la pagina
     *
     * @param request  request
     * @param response response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<String, Object> varValues = new HashMap<String, Object>(13);

        Usuario usuario = WebUtil.getUsuarioSesion(request);
        logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));
        Perfil perfil = null;
        if (usuario == null) {
            //lo enviamos a la pagina de login
            response.sendRedirect("index.jhtml");
            return;
        }  

        List<String> errores = new ArrayList<String>();
        if (request.getAttribute("errores") != null) errores = (List<String>) request.getAttribute("errores");

        int broadcastId = RequestUtil.getParameter(request, "id", -1);
        int broadcastIdClonar = RequestUtil.getParameter(request, "clonar", -1);
        String bcm = "bcm5";
        
        try {
            perfil = PerfilManagerWrapper.getPerfilForUser(usuario,bcm);
            logger.info("perfil for user "+perfil);
            
            logger.info("ClienteBroadcastManager.getInstance(" + bcm + ");");
            ClienteBroadcastManager cBCM = ClienteBroadcastManager.getInstance(bcm);
            logger.info("List<Conexion> connxns = cBCM.getConexiones()");
            logger.info("termino---");
        } catch (Throwable e) {
            e.printStackTrace();
        }

        try {
            ClienteBroadcastManager.getInstance(bcm);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        List<BCMConexionDO> listOfBcms = BCMConexionBO.getConexiones();

        //si es menor que 0 creamos un broadcast nuevo
        if (broadcastId < 0) {
            try {
                logger.info("BroadcastsManagerWrapper.addBroadcast");
                broadcastId = BroadcastsManagerWrapper.addBroadcast(usuario, null, perfil.getOperador_vdefecto(), "Nuevo Broadcast", "", new Date(), new Date(), 0, "", 1, perfil.getTasa_vdefecto(), Broadcast.Estado.NUEVO, Broadcast.BaseTipo.ARCHIVO, "", "", new Conexion("-", "-"), Broadcast.ListaNegraTipo.ARCHIVO, "", "", new Conexion("-", "-"), 1, bcm,"U",0,0,"", perfil.isValidar_lnu());
                logger.info("BroadcastsManagerWrapper.getBroadcastById(" + broadcastId + ", " + bcm + ")");
                Broadcast b = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);

                if (broadcastIdClonar < 0) {
                    //un broadcast nuevo tiene que tener al menos 1 mensaje
                    logger.info("MensajesManagerWrapper.addMensaje(" + b + ", " + perfil.getMensaje_tipo_vdefecto() + ", '', '', '', '', '', 0, " + Mensaje.Estado.GUARDADO + ", " + bcm + ")");
                    MensajesManagerWrapper.addMensaje(b, perfil.getMensaje_tipo_vdefecto(), perfil.getMensaje_servicio_vdefecto(), "0", perfil.getMensaje_la_vdefecto(), perfil.getMensaje_texto_vdefecto(),perfil.getMensaje_url_vdefecto(), 0, Mensaje.Estado.GUARDADO, bcm,perfil.getMensaje_sregala_vdefecto(),perfil.getMensaje_smsfinal_vdefecto());
                } else {
                    //si se pide clonar el broadcast, tomamos el broadcast a clonar y duplicamos su info en el nuevo.
                    logger.info("BroadcastsManagerWrapper.getBroadcastById(" + broadcastIdClonar + ", " + bcm + ")");
                    Broadcast bc = BroadcastsManagerWrapper.getBroadcastById(broadcastIdClonar, bcm);
                    //duplicamos los parametros basicos del broadcast
                    logger.info("BroadcastsManagerWrapper.updateBroadcast(b, bc.getOperador(), \"Copia de \" + bc.getNombre(), bc.getDescripcion(), bc.getFechaInicio(), bc.getFechaFin(), bc.getRepetirCada(), bc.getBaseControl(), bc.getBaseControlTasa(), bc.getEnviosPorSegundo(), Broadcast.Estado.NUEVO, \"\", bc.getBaseTipo(), bc.getBaseSql(), bc.getBaseDB(), bc.getListaNegraTipo(), bc.getListaNegraSql(), bc.getListaNegraDB(), bc.isAprobado(), bc.getSeparacionEntreMensajes(), bcm)");
                    BroadcastsManagerWrapper.updateBroadcast(b, bc.getOperador(), "Copia de " + bc.getNombre(), bc.getDescripcion(), bc.getFechaInicio(), bc.getFechaFin(), bc.getRepetirCada(), bc.getBaseControl(), bc.getBaseControlTasa(), bc.getEnviosPorSegundo(), Broadcast.Estado.NUEVO, "", bc.getBaseTipo(), bc.getBaseSql(), bc.getBaseDB(), bc.getListaNegraTipo(), bc.getListaNegraSql(), bc.getListaNegraDB(), bc.isAprobado(), bc.getSeparacionEntreMensajes(), bcm,bc.getDwTipoProducto(),bc.getDwTipoProductoId(),bc.getDwMedioId(),bc.getService_code());
                    
                    logger.info("BroadcastsManagerWrapper.updateBroadcastBaseArchivo(b, bc.getBasePathArchivo(), bcm)");
                    //TODO clonar - Clonacion con directorios independiente

                    int bid = b.getId();
                    String localBaseDirPath = "";
                    String nuevoPath = "";
                    String dirDestino = "";
                    
                    if (bc.getBaseTipo() == Broadcast.BaseTipo.ARCHIVO && bc.getBasePathArchivo()!= null && bc.getBasePathArchivo().length()> 1){
                    	String nombreBaseZip = bc.getBasePathArchivo().substring(bc.getBasePathArchivo().lastIndexOf("/")+1);
                        nuevoPath = "/"+ bid + "/" + nombreBaseZip;
                        
                        String pathBCM = "bcmf5";
                        localBaseDirPath = "/srv/produccion/data/broadcastManager/bases/"+pathBCM;
                        
                        dirDestino = "/"+ bid + "/";
                        File zipViejo = new File(localBaseDirPath+bc.getBasePathArchivo());
                        
                        if(zipViejo.exists() && !zipViejo.isDirectory()){
                            File dirNuevo = new File(localBaseDirPath+dirDestino);
                            FileUtils.copyFileToDirectory(zipViejo, dirNuevo);
                            
                            // Se asignan permisos para escritura sobre la carpeta para todos los usuarios.
                            try{
                                boolean isWritable = dirNuevo.setWritable(true, false);
                                if(!isWritable){
                                    logger.warn("No se tienen privilegios para cambiar permisos en la carpeta");
                                }
                            } catch (SecurityException se){
                                logger.error("Existe un Security Manager que no me permite modificar privilegios", se);
                            }
                            
                            logger.info("CLONACION | bc.getBasePathArchivo():"+bc.getBasePathArchivo()+" bid:"+b.getId());
                            logger.info("CLONACION | nuevoDir:"+dirNuevo+" bid:"+b.getId());
                            BroadcastsManagerWrapper.updateBroadcastBaseArchivo(b, nuevoPath, bcm);

                        } else{
                            logger.info("CLONACION | No posee zipViejo, o este es un directorio o es tipo SQL.");
                   
                        }
                    }else{
                    	logger.info("CLONACION | null o vacio basepatharchivo");
                    }
                    
                    //TODO NUEVA CLONACION LNU
                    
                    if (bc.getListaNegraTipo() == Broadcast.ListaNegraTipo.ARCHIVO && bc.getListaNegraPathArchivo() != null && bc.getListaNegraPathArchivo().length()> 1){
                    	String nombreLnZip = bc.getListaNegraPathArchivo().substring(bc.getListaNegraPathArchivo().lastIndexOf("/")+1);
                    	logger.info("BroadcastsManagerWrapper.updateBroadcastListaNegraArchivo(b, bc.getListaNegraPathArchivo(), bcm)");
                        File lnuViejo = new File(localBaseDirPath+bc.getListaNegraPathArchivo());
                        nuevoPath = "/"+ bid + "/" + nombreLnZip;
                        
                        if(lnuViejo.exists() && !lnuViejo.isDirectory()){
                            File dirNuevo = new File(localBaseDirPath+dirDestino);
                            FileUtils.copyFileToDirectory(lnuViejo, dirNuevo);
                            
                            // Se asignan permisos para escritura sobre la carpeta para todos los usuarios.
                            try{
                                boolean isWritable = dirNuevo.setWritable(true, false);
                                if(!isWritable){
                                    logger.warn("No se tienen privilegios para cambiar permisos en la carpeta");
                                }                                
                            } catch (SecurityException se){
                                logger.error("Existe un Security Manager que no me permite modificar privilegios", se);
                            }
                            
                            logger.info("CLONACION_LNU | bc.getListaNegraPathArchivo():"+bc.getListaNegraPathArchivo()+" bid:"+b.getId());
                            logger.info("CLONACION_LNU | nuevoDir:"+dirNuevo+" bid:"+b.getId());
                            BroadcastsManagerWrapper.updateBroadcastListaNegraArchivo(b, nuevoPath, bcm);
                        } else {
                            logger.info("CLONACION_LNU | No posee lnuViejo, o este es un directorio o es tipo sql");

                        }
                    }else{
                    	logger.info("CLONACION | null o vacio listanegrapatharchivo");
                    }

                    //duplicamos Mensajes
                    logger.info("MensajesManagerWrapper.getMensajeByBroadcast(bc, bcm)");
                    Mensaje mensaje = MensajesManagerWrapper.getMensajeByBroadcast(bc, bcm);

                    if (mensaje != null) {
                        logger.info("MensajesManagerWrapper.addMensaje(b, m.getTipo(), m.getServicio(), m.getPrecio(), m.getLa(), m.getTexto(), m.getUrl(), m.getOrden(), m.getEstado(), bcm)");
                        int i = MensajesManagerWrapper.addMensaje(b, mensaje.getTipo(), mensaje.getServicio(), mensaje.getPrecio(), mensaje.getLa(), mensaje.getTexto(), mensaje.getUrl(), mensaje.getOrden(), mensaje.getEstado(), bcm,mensaje.getServicioRegala(),mensaje.getSmsFinal());
                    }
                    

                    //Notificaciones
                    logger.info("NotificacionesManagerWrapper.getNotificacionesByBroadcast(bc, bcm)");
                    List<Notificacion> notificaciones = NotificacionesManagerWrapper.getNotificacionesByBroadcast(bc, bcm);
                    for (Notificacion n : notificaciones) {
                        if (n.getEstado().equals(Notificacion.Estado.GUARDADO)) {
                            logger.info("NotificacionesManagerWrapper.addNotificacion(b, n.getTipo(), n.getDestinatarios(), n.getCada(), n.getPerfil(), n.getEstado(), bcm)");
                            NotificacionesManagerWrapper.addNotificacion(b, n.getTipo(), n.getDestinatarios(), n.getCada(), n.getPerfil(), n.getEstado(), bcm);
                        }
                    }
                }

            } catch (SQLException e) {
                logger.error("Error al recuperar broadcast", e);
            }
        }


        Broadcast broadcast = null;
        Mensaje mensaje = null;
        List<ElementoMensaje> elementosMensajes = new ArrayList<ElementoMensaje>();
     
        List<Notificacion> notificaciones = new ArrayList<Notificacion>();
        try {
            logger.info("BroadcastsManagerWrapper.getBroadcastById(" + broadcastId + ", " + bcm + ")");
            broadcast = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);
            logger.info("MensajesManagerWrapper.getMensajeByBroadcast(" + broadcast + ", " + bcm + ")");
            mensaje = MensajesManagerWrapper.getMensajeByBroadcast(broadcast, bcm);
            
            logger.info("ElementosMensajesManagerWrapper.getElementosMensajesByMensaje( mensajes.get(0) , " + bcm + ") = " + mensaje);
            elementosMensajes = ElementosMensajesManagerWrapper.getElementosMensajesByMensaje(mensaje, bcm);
       
            logger.info("NotificacionesManagerWrapper.getNotificacionesByBroadcast(" + broadcast + ", " + bcm + ")");
            notificaciones = NotificacionesManagerWrapper.getNotificacionesByBroadcast(broadcast, bcm);
        } catch (SQLException e) {
            logger.error("Error al obtener el bradcast id " + broadcastId, e);
        }

        varValues.put("broadcast", broadcast);
        varValues.put("mensaje", mensaje);
        varValues.put("elementosMensajes", elementosMensajes);
        varValues.put("notificaciones", notificaciones);
        varValues.put("errores", errores);
        varValues.put("usuario", usuario);
        varValues.put("perfil", perfil);
        varValues.put("bcm", bcm);
        

        showTemplate(request, response, varValues);
    }

    /**
     * Procesa los datos que son actualizados, finalmente llama a doGet para que se despliegue la pagina.
     *
     * @param request  request
     * @param response response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String lala = RequestUtil.getParameter(request, "lala", null);


        Usuario usuario = WebUtil.getUsuarioSesion(request);
        logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));

        if (usuario == null) {
            //lo enviamos a la pagina de login
            response.sendRedirect("index.jhtml");
            return;
        }


        SimpleDateFormat f_yyyyMMdd_HHmm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        List<String> errores = new ArrayList<String>();
        Broadcast broadcast = null;

        int id = RequestUtil.getParameter(request, "id", -1);
        String nombre = RequestUtil.getParameter(request, "nombre", "");
        String descripcion = RequestUtil.getParameter(request, "descripcion", "");
        String operador = RequestUtil.getParameter(request, "operador", "");
        int tasa = RequestUtil.getParameter(request, "tasa", 1);
        int repetir = 0;

        boolean onSubmitGuardar = RequestUtil.getParameter(request, "submit", 0) == 1;
        String bcm = "bcm5";

        try {
            Broadcast.BaseTipo baseTipo = Broadcast.BaseTipo.valueOf(RequestUtil.getParameter(request, "base_tipo", "ARCHIVO"));
            Broadcast.ListaNegraTipo listaNegraTipo = Broadcast.ListaNegraTipo.valueOf(RequestUtil.getParameter(request, "ln_tipo", "ARCHIVO"));
            broadcast = BroadcastsManagerWrapper.getBroadcastById(id, bcm);

            Operador operadorShare = Operador.valueOf(operador);
            
            Date fechaInicio;
            try {
                String sFechaInicio = RequestUtil.getParameter(request, "fecha_inicio", "");
                sFechaInicio = WebUtil.tranformaTimeZoneaUTC(sFechaInicio, "yyyy-MM-dd HH:mm", operadorShare, "yyyy-MM-dd HH:mm");
                fechaInicio = f_yyyyMMdd_HHmm.parse(sFechaInicio);
            } catch (ParseException e) {
                fechaInicio = WebUtil.getFechaInicioDefault();
                errores.add("Error en fecha de inicio de broadcast, se asigno fecha por defecto.");
            }
            Date fechaFin;
            try {
                //fechaFin = f_yyyyMMdd_HHmm.parse(RequestUtil.getParameter(request, "fecha_fin", ""));
                String sFechaFin = RequestUtil.getParameter(request, "fecha_fin", "");
                sFechaFin= WebUtil.tranformaTimeZoneaUTC(sFechaFin, "yyyy-MM-dd HH:mm", operadorShare, "yyyy-MM-dd HH:mm");
                fechaFin = f_yyyyMMdd_HHmm.parse(sFechaFin);
            } catch (ParseException e) {
                fechaFin = WebUtil.getFechaFinDefault();
                errores.add("Error en fecha de fin de broadcast, se asigno fecha por defecto.");
            }

            //TODO: base de control y base control tasa
            
            BroadcastsManagerWrapper.updateBroadcast(broadcast, operadorShare, nombre, descripcion, fechaInicio, fechaFin, repetir, "", 1, tasa, broadcast.getEstado(), broadcast.getEstadoDetalle(), baseTipo, "", null, listaNegraTipo, "", null, false, 0, bcm,"U",7777,8888,"");

            
            //actualizamos los datos del mensaje
            String mensajeTipo = RequestUtil.getParameter(request, "mensaje_tipo", "SMS");
            //Siempre seran 2 elementos independiente del tipo de mensaje
            String mensajesTitulo[] = request.getParameterValues("mensaje_titulo");
            String mensajeLA = RequestUtil.getParameter(request, "mensaje_la", "").trim();
            String mensajeServicio = RequestUtil.getParameter(request, "mensaje_servicio", "").trim();
            String mensajeUrl = RequestUtil.getParameter(request, "mensaje_url", "");
            String servicioRegala = RequestUtil.getParameter(request, "servicio_rega", "").trim();
            String smsFinal =  RequestUtil.getParameter(request, "sms_final", "").trim();
            String mensajeSubject = RequestUtil.getParameter(request, "mensaje_subject", "").trim();
            String mensajePrecio = RequestUtil.getParameter(request, "mensaje_precio", "").trim();
            Mensaje mensaje = MensajesManagerWrapper.getMensajeByBroadcast(broadcast, bcm);
            List<ElementoMensaje> elementosMensajes = ElementosMensajesManagerWrapper.getElementosMensajesByMensaje(mensaje, bcm);

            if ((mensajeServicio.equals("") || mensajeLA.equals("")) && Mensaje.Tipo.REGALA != Mensaje.Tipo.valueOf(mensajeTipo) ) {
                logger.warn("doPost->No se puede utilizar un Servicio o LA vacio. {broadcastId,mensajeId}: {" + broadcast.getId() + "," + mensaje.getId() + "}");
                errores.add("No se puede utilizar un Servicio o LA vacio. Por favor actualizar los campos");
            }
            //4 Tipos de mensajes: SMS(1), SMS(2), WAPPUSH y MMS
            try {
                logger.info("doPost->Se actualiza mensajeId:" + mensaje.getId());
                String txt = "";
                if (Mensaje.Tipo.MMS == Mensaje.Tipo.valueOf(mensajeTipo)) {
                    txt = mensajeSubject;
                }else{
                    txt = mensajesTitulo[0];
                }

                MensajesManagerWrapper.updateMensaje(mensaje, Mensaje.Tipo.valueOf(mensajeTipo), mensajeServicio, mensajePrecio, mensajeLA, txt, mensajeUrl, mensaje.getOrden(), Mensaje.Estado.GUARDADO, bcm,servicioRegala,smsFinal);
            } catch (Exception ei) {
                errores.add("Error al guardar datos de mensaje (e=" + ei.getMessage() + ")");
            }
            
            if (Mensaje.Tipo.MMS == Mensaje.Tipo.valueOf(mensajeTipo)) {
                int elementoMensajeIndex = 0;
                String mensajeError = null;
                for (ElementoMensaje elementoMensaje : elementosMensajes) {
                    mensajeError = null;
                    if (ElementoMensaje.Estado.ELIMINADO == elementoMensaje.getEstado()) {
                        continue;
                    }
                    //Si se elimino el mensaje desde la pantalla antes de ejecutar guardar
                    if (null == request.getParameter("elementoMensaje_duracion_" + elementoMensaje.getId())) {
                        logger.info("doPost->Se va a eliminar el elemento mensajeId (Se elimino desde pantalla):" + elementoMensaje.getId());
                        ElementosMensajesManagerWrapper.deleteElementoMensaje(elementoMensaje, bcm);
                        continue;
                    }
                    elementoMensajeIndex++;
                    //Se coloca estado "nuevo" por defecto:
                    elementoMensaje.setEstado(ElementoMensaje.Estado.NUEVO);
                    elementoMensaje.setDuracion(RequestUtil.getParameter(request, "elementoMensaje_duracion_" + elementoMensaje.getId(), -1));
                    if (ElementoMensaje.Tipo.TEXT == elementoMensaje.getTipo()) {
                        elementoMensaje.setTexto(RequestUtil.getParameter(request, "elementoMensaje_texto_" + elementoMensaje.getId(), null));
                        if (null == elementoMensaje.getTexto()) {
                            mensajeError = "Elemento Nro " + elementoMensajeIndex + " del MMS: duracion es incorrecto o texto en blanco";
                        }
                    } else if (ElementoMensaje.Tipo.IMG == elementoMensaje.getTipo()) {
                        if (null == elementoMensaje.getUrl()) {
                            mensajeError = "Elemento Nro " + elementoMensajeIndex + " del MMS: duracion es incorrecto o no subio imagen";
                        }
                    }

                    //Para que quede como "guardado" depende del tipo  de mensaje ademas de la duracion
                    if (0 <= elementoMensaje.getDuracion()) {
                        if (ElementoMensaje.Tipo.TEXT == elementoMensaje.getTipo() && null != elementoMensaje.getTexto()) {
                            elementoMensaje.setEstado(ElementoMensaje.Estado.GUARDADO);
                        } else if (ElementoMensaje.Tipo.IMG == elementoMensaje.getTipo() && null != elementoMensaje.getUrl()) {
                            elementoMensaje.setEstado(ElementoMensaje.Estado.GUARDADO);
                        }
                    } else if (null == mensajeError) {
                        mensajeError = "Elemento Nro " + elementoMensajeIndex + " del MMS: duracion es incorrecto";
                    }
                    if (null != mensajeError) {
                        logger.warn("doPost->" + mensajeError + ". broadcatId:" + broadcast.getId());
                        errores.add(mensajeError);
                    }
                    logger.info("doPost->Se guarda el elementoMensaje: " + elementoMensaje);
                    ElementosMensajesManagerWrapper.updateElementoMensaje(elementoMensaje, elementoMensaje.getTexto(), elementoMensaje.getDuracion(), elementoMensaje.getEstado(), bcm);
                }
                if (0 == elementoMensajeIndex) {
                    logger.warn("doPost->No agrego elementos al mensaje MMS:" + mensaje.getId());
                    errores.add("Para mensaje de tipo MMS debe agregar al menos un elemento");
                }
            } else {
                //Se debe eliminar todos los elementos mensajes. (anteriormenste se pudo configurar como tipo MMS)
                for (ElementoMensaje elementoMensaje : elementosMensajes) {
                    ElementosMensajesManagerWrapper.deleteElementoMensaje(elementoMensaje, bcm);
                }
            }

            //NOTIFICACIONES
            List<Notificacion> notificaciones = NotificacionesManagerWrapper.getNotificacionesByBroadcast(broadcast, bcm);
            List<Notificacion> notificacionesEliminar = new ArrayList<Notificacion>();
            for (Notificacion notificacion : notificaciones) {
                if (notificacion.getEstado().equals(Notificacion.Estado.ELIMINADO)) {
                    continue;
                }

                String notDestinatarios = RequestUtil.getParameter(request, "notificacion_destinatarios_" + notificacion.getId(), null);
                String notPerfil = RequestUtil.getParameter(request, "notificacion_perfil_" + notificacion.getId(), "");
                String notTipo = RequestUtil.getParameter(request, "notificacion_tipo_" + notificacion.getId(), "");
                int notCada = RequestUtil.getParameter(request, "notificacion_cada_" + notificacion.getId(), 1000);
                if (notDestinatarios == null) {
                    notificacionesEliminar.add(notificacion);
                } else {
                    try {
                        NotificacionesManagerWrapper.updateNotficacion(notificacion, Notificacion.Tipo.valueOf(notTipo), notDestinatarios.trim(), notCada, Notificacion.Perfil.valueOf(notPerfil), Notificacion.Estado.GUARDADO, bcm);
                    } catch (Exception ei) {
                        String guardadaActualizada = notificacion.getEstado() == Notificacion.Estado.NUEVO ? "guardada" : "actualizada";
                        errores.add("Error al notificacion " + notDestinatarios + ". Notificacion no " + guardadaActualizada + " (e=" + ei.getMessage() + ")");
                    }
                }
            }
            for (Notificacion notificacion : notificacionesEliminar) {
                NotificacionesManagerWrapper.deleteNotificacion(notificacion, bcm);
            }
        } catch (Exception e) {
            logger.error("Error al guardar broadcast", e);
            logger.info("Error al guardar broadcast", e);
            errores.add("Error inesperador al guardar broadcast, por favor reingrese a la pagina de edicion. (e=" + e.getCause() + ")");
        }

        //si no hay errores y el broadcast es nuevo, lo dejamos como guardado
        if (errores.size() == 0 && broadcast != null && broadcast.getEstado().equals(Broadcast.Estado.NUEVO) && onSubmitGuardar) {
            try {
                BroadcastsManagerWrapper.updateBroadcastEstado(broadcast, Broadcast.Estado.GUARDADO, "", bcm);
            } catch (SQLException e) {
                errores.add("Error al actualizar estado de broadcast " + e.getMessage());
            }
        }

        request.setAttribute("errores", errores);
        doGet(request, response);
    }

    public static void logea(String str) {
        logger.info(str);
    }

}
