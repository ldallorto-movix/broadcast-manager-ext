package com.movixla.broadcastmanager.servlet;

import com.movix.shared.util.Configuracion;
import com.movix.shared.util.RequestUtil;
import com.movix.shared.util.Util;
import com.movix.shared.wap.WapUtil;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.ElementoMensaje;
import com.movixla.broadcastmanager.aplicacion.FileUploadListener;
import com.movixla.broadcastmanager.aplicacion.WebUtil;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.BroadcastsManagerWrapper;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.ElementosMensajesManagerWrapper;
import com.movixla.broadcastmanager.base.AppWebBaseServlet;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Andres
 * Date: 02-jun-2009
 * Time: 10:46:50
 */
public class FileUploadServlet extends AppWebBaseServlet {
    public static String BASE_BASE = "base";
    public static String BASE_NEGRA = "negra";
    public static String ELEMENTO_MENSAJE_IMAGEN = "elementoMensajeImagen";

    /**
     * Despliega la pagina
     *
     * @param request  request
     * @param response response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<String, Object> varValues = new HashMap<String, Object>(4);
        String tipo = RequestUtil.getParameter(request, "tipo", "");
        int broadcastId = RequestUtil.getParameter(request, "broadcast_id", -1);
        String bcm = "bcm5";
   
        int elementoId = RequestUtil.getParameter(request, "elementoId", -1);
        
        varValues.put("tipo", tipo);
        varValues.put("broadcastId", broadcastId);
        varValues.put("elementoId", elementoId);
        varValues.put("bcm", bcm);
   
        showTemplate(request, response, varValues);
    }


    /**
     * Recibe el archivo
     *
     * @param request  request
     * @param response response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {


            String status = RequestUtil.getParameter(request, "status", null);
            String tipo = RequestUtil.getParameter(request, "tipo", null); //base o negra
            int broadcastId = RequestUtil.getParameter(request, "broadcast_id", -1); //broadcast_id
            String bcm = "bcm5";

            int elementoId = RequestUtil.getParameter(request, "elementoId", -1); //Para identificador elemento de mensaje de tipo MMS
            
            String filePathBase = Configuracion.get(bcm + ".path_bases");
            File baseDir = new File(filePathBase);


            Usuario usuario = WebUtil.getUsuarioSesion(request);
            logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));

            HttpSession session = request.getSession();
            if (status != null) {
                //estan preguntando por el status del upload.. cuando se ha subido hasta el momento.. etc.
                FileUploadListener listener = null;

                long bytesRead = 0L;
                long contentLength = 0L;
                long uploadStatus = 0;
                String file = "";
                String badLine = "";


                if (session != null) {
                    // Check to see if we've created the listener object yet
                    listener = (FileUploadListener) session.getAttribute("LISTENER" + tipo + "_" + broadcastId);
                    if (listener != null) {
                        // Get the meta information
                        bytesRead = listener.getBytesRead();
                        contentLength = listener.getContentLength();
                        badLine = listener.getBadline();
                        file = listener.getFile() != null ? getRelativePath(baseDir, listener.getFile()) : "";
                        if (listener.getStatus().equals(FileUploadListener.Status.UPLOADING)) {
                            uploadStatus = 1;
                        } else if (listener.getStatus().equals(FileUploadListener.Status.CHECKING)) {
                            uploadStatus = 2;
                        } else if (listener.getStatus().equals(FileUploadListener.Status.FINISHED)) {
                            uploadStatus = 3;
                        } else {
                            uploadStatus = 4;
                        }
                    }
                }

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("filesize", contentLength);
                jsonObject.put("uploaded", bytesRead);
                jsonObject.put("uploadstatus", uploadStatus);
                jsonObject.put("file", file);
                jsonObject.put("badline", badLine);

                logger.info("json=" + jsonObject.toString());
                response.getOutputStream().print(jsonObject.toString());

            } else {
                //estan enviando el archivo
                FileUploadListener listener = new FileUploadListener();
                session.setAttribute("LISTENER" + tipo + "_" + broadcastId, listener);
                try {
                    logger.info("uploading");
                    // set file upload progress listener

                    // create file upload factory and upload servlet
                    FileItemFactory factory = new DiskFileItemFactory();
                    ServletFileUpload upload = new ServletFileUpload(factory);

                    // upload servlet allows to set upload listener
                    upload.setProgressListener(listener);

                    List uploadedItems = null;
                    FileItem fileItem = null;
                    String filePath = filePathBase + File.separator + broadcastId;// File.separator + tipo2 + "_" + format.format(fechaUpload);
                    File parentDir = new File(filePath);
                    if (!parentDir.exists()) {
                        parentDir.mkdirs();
                        
                        // Se asignan permisos para escritura sobre la carpeta para todos los usuarios.
                        try{
                            boolean isWritable = parentDir.setWritable(true, false);
                            if(!isWritable){
                                logger.warn("No se tienen privilegios para cambiar permisos en la carpeta");
                            }                            
                        } catch (SecurityException se){
                            logger.error("Existe un Security Manager que no me permite modificar privilegios", se);
                        }
                    }

                    logger.info("filePath=" + filePath);

                    // iterate over all uploaded files
                    uploadedItems = upload.parseRequest(request);

                    Iterator i = uploadedItems.iterator();

                    while (i.hasNext()) {
                        fileItem = (FileItem) i.next();

                        if (fileItem.isFormField() == false) {
                            if (fileItem.getSize() > 0) {
                                File uploadedFile = null;
                                String myFullFileName = fileItem.getName();
                                logger.info("myFullFileName=" + myFullFileName);
                                String myFileName = "";
                                String slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/"; // Windows or UNIX

                                int startIndex = myFullFileName.lastIndexOf(slashType);

                                // Ignore the path and get the filename
                                myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());

                                // Create new File object

                                uploadedFile = new File(filePath, myFileName);
                                System.out.println("uploadedFile = " + uploadedFile.getPath());

                                // Write the uploaded file to the system
                                fileItem.write(uploadedFile);

                                //actualizamos la base de datos

                                try {

                                    logger.info("uploadedFile = " + uploadedFile);

                                    String check_msg = "";
                                    listener.setBadline("");
                                    listener.setStatus(FileUploadListener.Status.CHECKING);

                                    if (ELEMENTO_MENSAJE_IMAGEN.equals(tipo)) {
                                        logger.info("doPost->elementoMensajeId:" + elementoId);
                                        ElementoMensaje elementoMensaje = ElementosMensajesManagerWrapper.getElementoMensajeById(elementoId, bcm);
                                        logger.info("doPost->Actualizando URL:" + getRelativePath(baseDir, uploadedFile));
                                        ElementosMensajesManagerWrapper.updateURL(elementoMensaje, getRelativePath(baseDir, uploadedFile), bcm);
                                        listener.setFile(uploadedFile);
                                        listener.setStatus(FileUploadListener.Status.FINISHED);
                                        session.setAttribute("LISTENER" + tipo + "_" + broadcastId, listener);

                                    } else {
                                        
                                        try {
                                            if (myFileName.toUpperCase().endsWith(".ZIP")){
                                                check_msg = ""; 
                                                if (myFileName.contains(" ")){
                                                    check_msg = "Verificar que el archivo zipeado y el archivo de texto NO tengan espacios en el nombre";
                                                }

                                            }else{
                                                check_msg = WebUtil.checkBase(uploadedFile);
                                            }
                                        } catch (Exception eee) {
                                            check_msg = "Exception checkBase";
                                            logger.error("Error al checkear base " + uploadedFile.getPath(), eee);
                                        }

                                        if (check_msg.equals("")) {
                                            Broadcast broadcastById = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);
                                            if (tipo.equals(BASE_BASE)) {
                                                BroadcastsManagerWrapper.updateBroadcastBaseArchivo(broadcastById, getRelativePath(baseDir, uploadedFile), bcm);
                                            } else if (tipo.equals(BASE_NEGRA)) {
                                                BroadcastsManagerWrapper.updateBroadcastListaNegraArchivo(broadcastById, getRelativePath(baseDir, uploadedFile), bcm);
                                            }

                                            listener.setFile(uploadedFile);
                                            listener.setStatus(FileUploadListener.Status.FINISHED);
                                            session.setAttribute("LISTENER" + tipo + "_" + broadcastId, listener);
                                        } else {
                                            logger.error("La base no corresponde a una base valida: " + uploadedFile.getPath());
                                            listener.setStatus(FileUploadListener.Status.ERROR);
                                            listener.setBadline(check_msg);
                                        }
                                        
                                    }
                                   

                                } catch (Exception esql) {
                                    logger.error("error ", esql);
                                    listener.setStatus(FileUploadListener.Status.ERROR);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("Error al subir archivo tipo=" + tipo + ", broadcast_id=" + broadcastId, e);
                }

            }

        } catch (Throwable ttt) {
            logger.error("no pensaste en algo", ttt);


        }
    }

    private static String getRelativePath(File base, File destino) {
        return destino.getPath().replace(base.getPath(), "");
    }
}
