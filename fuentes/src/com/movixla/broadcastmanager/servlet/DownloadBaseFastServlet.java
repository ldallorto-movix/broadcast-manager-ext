package com.movixla.broadcastmanager.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import com.movix.shared.util.Configuracion;
import com.movix.shared.util.EnviadorArchivo;
import com.movix.shared.util.RequestUtil;
import com.movix.shared.util.Util;
import com.movix.shared.wap.WapUtil;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.WebUtil;
import com.movixla.broadcastmanager.base.AppWebBaseServlet;


/**
 * Created by Andres
 * Date: 17-dic-2009
 * Time: 12:07:58
 */
public class DownloadBaseFastServlet extends AppWebBaseServlet {
    
    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(DownloadBaseFastServlet.class);
    
    
    private final static String BASE_COMPLETA                   = "ALL";
    private final static String BASE_ENVIADA                    = "ENVIADO";
    private final static String BASE_NO_ENVIADA                 = "NO_ENVIADO";
    private final static String NUMEROS_BASE_LNU                = "NUMEROS_BASE_EN_LNU";
    private final static String NUMEROS_BASE_LISTA_NEGRA        = "NUMEROS_BASE_EN_LISTA_NEGRA";
    
    
    private final static String SHELL_CAT4          = "sh /srv/produccion/scripts/cat4.sh ";
    private final static String SHELL_CAT_UNZIP4    = "sh /srv/produccion/scripts/catunzip4.sh ";
    private final static String SHELL_CAT3          = "sh /srv/produccion/scripts/cat3.sh ";
    private final static String SHELL_CAT_UNZIP3    = "sh /srv/produccion/scripts/catunzip3.sh ";

    
    static {
    }

    /*
     * doGet del servicio http.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String downloadedFile = null;

        String req = RequestUtil.getParameter(request, "req", "");
        String bcm = RequestUtil.getParameter(request, "bcm", "");
        String bcmHost = Configuracion.get(bcm + ".contenedorBroadcast.host");

        logger.info("Parametros: req: {} bcm:{} bcmHost:{}", req, bcm, bcmHost);

        Usuario usuario = WebUtil.getUsuarioSesion(request);
        logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));

        if (usuario == null) {
            response.sendRedirect("login.jhtml");
        }

        int bid = RequestUtil.getParameter(request, "broadcast_id", -1);
        String estado = RequestUtil.getParameter(request, "estado", null);

        // voy a hacer la consulta aqui para que sea mas rapido y no gastar tanta memoria en objetos.
        if (bid > 0 && estado != null) {
        	
        	String pathBCM = "bcmf5";
            
            String localFolderPath = "/srv/produccion/data/broadcastManager/bases/"+pathBCM+"/" + bid + "/";

            File fileAux = new File(localFolderPath);
            logger.info("[{}] FolderPath: {}", bid, fileAux.getAbsolutePath());
            if(fileAux.exists() && fileAux.isDirectory() && fileAux.listFiles().length > 0){
                logger.info("[{}] es un directorio", bid);
                
            }else{
                logger.warn("No existe el directorio base o no tiene archivos"+localFolderPath+"'");
                return;
            }
            
            String emptyFile = "/srv/produccion/scripts/empty.txt";

            // Descarga base completa
            if (estado.equalsIgnoreCase(BASE_COMPLETA)) {
                logger.info("Descarga base completa");
                
                //se debe concatenar todos los archivos para obtener la base completa
                String baseFile1 = "enviados.txt";
                String baseFile2 = "noenviados.txt";
                String baseFile3 = "baseenlnu.txt";
                String baseFile4 = "baseenlnp.txt";
                
                boolean fileExists = (checkFile(baseFile1, localFolderPath) && checkFile(baseFile2, localFolderPath) && checkFile(baseFile3, localFolderPath));
                boolean fileExists4 = checkFile(baseFile4, localFolderPath);
                
                String shell = "";
                if (fileExists4) {
                	shell = SHELL_CAT4;
                }else{
                	shell = SHELL_CAT3;
                }
                if (!fileExists) {
                    logger.warn("Los archivos bases no existen, se buscaran los Zipeados");
                    
                    baseFile1 = baseFile1+".zip";
                    baseFile2 = baseFile2+".zip";
                    baseFile3 = baseFile3+".zip";
                    baseFile4 = baseFile4+".zip";
                    
                    if (!checkFile(baseFile1, localFolderPath)){
                        baseFile1 = "/srv/produccion/scripts/empty.txt.zip";
                    }
                    boolean zippedFileExists = (checkFile(baseFile2, localFolderPath) && checkFile(baseFile3, localFolderPath));
                    
                    if(!zippedFileExists){
                        logger.warn("Faltan los archivos necesarios para la obtencion de la Base_Completa");
                        return;
                    }
                    fileExists4 = checkFile(baseFile4, localFolderPath);
                    
                    if (fileExists4) {
                    	shell = SHELL_CAT_UNZIP4;
                    }else{
                    	shell = SHELL_CAT_UNZIP3;
                    }
                    
                }
                
                if (fileExists4){
                	downloadedFile = generateBaseFile( localFolderPath,"basetotal.txt", bid, shell,  baseFile1, baseFile2, baseFile3, baseFile4,localFolderPath + "basetotal.txt" );
                }else{
                	downloadedFile = generateBaseFile( localFolderPath, "basetotal.txt", bid, shell,  baseFile1, baseFile2, baseFile3,localFolderPath + "basetotal.txt" );
                }
            }
            else if (estado.equalsIgnoreCase(BASE_ENVIADA)) {
                // Descarga base enviada
                logger.info("Descarga base enviada");
                String baseFileName = "enviados.txt";
                
                boolean fileExists = checkFile(baseFileName, localFolderPath);
                if(!fileExists){
                    logger.warn("No existe la base folderPath:'"+localFolderPath+"' || baseFileName:'"+baseFileName+"', se buscará zipeado");
                    baseFileName = baseFileName+".zip";
                    
                    boolean zippedFileExists = checkFile(baseFileName, localFolderPath);
                    if(!zippedFileExists){
                        logger.warn("No existe la base: '"+baseFileName+"'");
                        downloadedFile = emptyFile;
                    }else{
                        downloadedFile = localFolderPath + baseFileName;
                    }
                }else{

                    downloadedFile = localFolderPath + baseFileName;
                }

            }
            else if (estado.equalsIgnoreCase(BASE_NO_ENVIADA)) {
                // Descarga base no enviada
                logger.info("Descarga base no enviada");
                
                String FileName = "noenviados.txt";
                boolean fileExists = checkFile(FileName, localFolderPath);
              
                if (!fileExists) {
                    logger.warn("noenviados no existe, se busca el zipeado");
                    
                    FileName = FileName+".zip";
                    
                    boolean zippedFileExists = checkFile(FileName, localFolderPath);
                    if(!zippedFileExists){
                        logger.warn("No existe noenviados: '"+FileName+"'");
                        downloadedFile = emptyFile;
                    }else{
                        downloadedFile = localFolderPath + FileName;
                    }
                    
                }else{
                    downloadedFile = localFolderPath + FileName;
                }

            }
            else if (estado.equalsIgnoreCase(NUMEROS_BASE_LNU)) {
                // Descarga Numeros de la Base que estan en LNU
                logger.info("Descarga Numeros de la Base que estan en LNU");

                String FileName = "baseenlnu.txt";
                boolean fileExists = checkFile(FileName, localFolderPath);
              
                if (!fileExists) {
                    logger.warn("baseenlnu no existe, se busca el zipeado");
                    
                    FileName = FileName+".zip";
                    
                    boolean zippedFileExists = checkFile(FileName, localFolderPath);
                    if(!zippedFileExists){
                        logger.warn("No existe baseenlnu: '"+FileName+"'");
                        downloadedFile = emptyFile;
                    }else{
                        downloadedFile = localFolderPath + FileName;
                    }
                    
                }else{
                    downloadedFile = localFolderPath + FileName;
                }

            }
            else if (estado.equalsIgnoreCase(NUMEROS_BASE_LISTA_NEGRA)) {
                // Descarga Numeros de la Base que estan en Lista Negra
                logger.info("Descarga Numeros de la Base que estan en Lista Negra");
                
                String FileName = "baseenlnp.txt";
                boolean fileExists = checkFile(FileName, localFolderPath);
              
                if (!fileExists) {
                    logger.warn("baseenlnp no existe, se busca el zipeado");
                    
                    FileName = FileName+".zip";
                    
                    boolean zippedFileExists = checkFile(FileName, localFolderPath);
                    if(!zippedFileExists){
                        logger.warn("No existe baselistanegra: '"+FileName+"'");
                        downloadedFile = emptyFile;
                    }else{
                        downloadedFile = localFolderPath + FileName;
                    }
                    
                }else{
                    downloadedFile = localFolderPath + FileName;
                }
                
                

            }
            
            if(downloadedFile != null){
                File file = new File(downloadedFile);            
                EnviadorArchivo enviador = new EnviadorArchivo(4096);
                enviador.enviar(response, file, EnviadorArchivo.CONTENT_TYPE_TEXT, EnviadorArchivo.DISPOSITION_ATTACHMENT, false);
            }

        } else {

        }
    }
    
    private boolean checkFile(String fileName, String folderPath){
        
        File file = new File(folderPath+ fileName);
        
        return file.exists();
        
    }
    
    private String generateBaseFile(String folderPath, String resultFileName, int bid, String shell, String... params) throws IOException {
        String result = null;
        String command = shell + folderPath + " "; 

        for (String string : params) {
            command = command + string + " ";
        }
        logger.info("command: {}", command);
        
        Process p = Runtime.getRuntime().exec(command);
        
        try{
            p.waitFor();
        }catch(InterruptedException e){
            
        }

        if (0 != p.exitValue()) {
            logger.warn("status="+p.exitValue()+ " "+p.getErrorStream().toString());
            return null;
        }
        
        return folderPath+resultFileName;
    }
    
}
