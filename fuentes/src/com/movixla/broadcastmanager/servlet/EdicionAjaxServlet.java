package com.movixla.broadcastmanager.servlet;

import com.movix.shared.Operador;
import com.movix.shared.db.DBManager;
import com.movix.shared.util.Configuracion;
import com.movix.shared.util.RequestUtil;
import com.movix.shared.util.Util;
import com.movix.shared.wap.WapUtil;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcast.shared.Accion;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.ElementoMensaje;
import com.movixla.broadcast.shared.Notificacion;
import com.movixla.broadcast.shared.Restriccion;
import com.movixla.broadcast.shared.manager.ConexionManager;
import com.movixla.broadcastmanager.aplicacion.ClienteBroadcastManager;
import com.movixla.broadcastmanager.aplicacion.WebUtil;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.*;
import com.movixla.broadcastmanager.base.AppWebBaseServlet;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by Andres
 * Date: 29-may-2009
 * Time: 18:39:24
 */
public class EdicionAjaxServlet extends AppWebBaseServlet {
    private static final String REQ_AGREGAR = "agregar";
    private static final String REQ_AGREGAR_NOTIFICACION = "notificacion";
    private static final String REQ_AGREGAR_ELEMENTO_MENSAJE_TEXTO = "elementoMensajeTexto";
    private static final String REQ_AGREGAR_ELEMENTO_MENSAJE_IMAGEN = "elementoMensajeImagen";
    private static final String REQ_ELIMINARLISTANEGRA = "eliminarlistanegra";


    private static final String REQ_LA = "la";
    private static final String REQ_FECHA = "fecha";
    private static final String REQ_FECHA_UTC = "fechautc";
    /*
     * doPost del servicio http.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String req = RequestUtil.getParameter(request, "req", "");
        String bcm = RequestUtil.getParameter(request, "bcm", "");


        int id = -1;
        String resultado = "";
        String error = "";

        Usuario usuario = WebUtil.getUsuarioSesion(request);
        logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));

        if (usuario == null) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", -1);
            jsonObject.put("resultado", "");
            jsonObject.put("error", "Sesion expirada.");
            logger.info(jsonObject.toString());
            response.getOutputStream().print(jsonObject.toString());
            return;
        }

        if (req.equals(REQ_AGREGAR)) {
            
            logger.info("req.equals(" + REQ_AGREGAR + ")");
            String tipo = RequestUtil.getParameter(request, "tipo", "");
            int broadcastId = RequestUtil.getParameter(request, "broadcast", -1);

            try {
                logger.info("BroadcastsManagerWrapper.getBroadcastById(" + broadcastId + ", " + bcm + ")");
                Broadcast broadcast = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);

                if (WebUtil.isInactivo(broadcast)) {
                    id = -1;
                    error = "El broadcast ya no esta activo. No se pudo realizar la accion solicitada.";
                } else {
//                    if (tipo.equals(REQ_AGREGAR_ACCION) && WebUtil.isEnProceso(broadcast)) {
//                        id = -1;
//                        error = "El broadcast esta en proceso. No se pudo realizar la accion solicitada.";
//                    } else 
                    if (tipo.equals(REQ_AGREGAR_NOTIFICACION)) {
                        logger.info("tipo.equals(" + REQ_AGREGAR_NOTIFICACION + ")");
                        Notificacion dummy = WebUtil.getNotificacionDummy(broadcast);
                        id = NotificacionesManagerWrapper.addNotificacion(broadcast, dummy.getTipo(), dummy.getDestinatarios(), dummy.getCada(), dummy.getPerfil(), Notificacion.Estado.NUEVO, bcm);
                        logger.info("Salio de tipo.equals(" + REQ_AGREGAR_NOTIFICACION + ")");
                    } else if (tipo.equals(REQ_AGREGAR_ELEMENTO_MENSAJE_TEXTO) || tipo.equals(REQ_AGREGAR_ELEMENTO_MENSAJE_IMAGEN)) {
                        logger.info("tipo.equals(" + REQ_AGREGAR_ELEMENTO_MENSAJE_TEXTO + ")");
                        Mensaje mensaje = MensajesManagerWrapper.getMensajeByBroadcast(broadcast, bcm);
                        ElementoMensaje dummy = WebUtil.getElementoMensajeTextoDummy(mensaje);
                        logger.info("Salio de tipo.equals(" + REQ_AGREGAR_ELEMENTO_MENSAJE_TEXTO + ")");
                        if (tipo.equals(REQ_AGREGAR_ELEMENTO_MENSAJE_IMAGEN)) {
                            logger.info("tipo.equals(" + REQ_AGREGAR_ELEMENTO_MENSAJE_IMAGEN + ")");
                            dummy = WebUtil.getElementoMensajeImagenDummy(mensaje);
                            logger.info("Salio de tipo.equals(" + REQ_AGREGAR_ELEMENTO_MENSAJE_IMAGEN + ")");
                        }
                        id = ElementosMensajesManagerWrapper.addElementoMensaje(mensaje, dummy.getTipo(), dummy.getTexto(), dummy.getUrl(), dummy.getDuracion(), dummy.getEstado(), bcm);
                    } else {
                        id = -1;
                        error = "Tipo de peticion '" + tipo + "' no puedo es reconocida.";
                    }
                }

            } catch (Exception e) {
                logger.error("Error en EdicionAjax para tipo=" + tipo + ", broadcastId=" + broadcastId, e);
                id = -1;
                error = "Ha ocurrido un error al procesar la peticion: " + e.getMessage();
            }
        } else if (req.equals(REQ_LA)) {
            String opString = RequestUtil.getParameter(request, "operador", "");
            String la = RequestUtil.getParameter(request, "la", "").trim();

            String servicio;

            try {
                logger.info("Operador.valueOf(" + opString + ")");
                Operador operador = Operador.valueOf(opString);
                //servicio = Configuracion.get(operador.name() + "." + la);

                int broadcastId = RequestUtil.getParameter(request, "broadcast", -1);
                logger.info("BroadcastsManagerWrapper.getBroadcastById(" + broadcastId + ", " + bcm + ")");
                Broadcast broadcast = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);

                logger.info("MensajesManagerWrapper.getMensajeByBroadcast(broadcast, bcm).size() => " + MensajesManagerWrapper.getMensajeByBroadcast(broadcast, bcm));
                Mensaje mensaje = MensajesManagerWrapper.getMensajeByBroadcast(broadcast, bcm);
                ElementoMensaje elementoMensaje = WebUtil.getElementoMensajeTextoDummy(mensaje);

                //servicio = Configuracion.get(operador.getIdBD()+ "." + la + "." + elementoMensaje.getTipo());
                logger.info("getServicioFromBD(" + operador + ", " + la + ", " + elementoMensaje + ") => ");
                servicio = getServicioFromBD(operador, la, elementoMensaje);
                logger.debug("Se retorno el servicio sugerido : " + servicio);
                if (servicio == null) {
                    servicio = Configuracion.get(operador.name() + ".*");
                    if (servicio == null) {
                        error = "No se encontro un servicio para el operador y LA especificado. Por favor pida un nuevo servicio al area de desarrollo";
                        servicio = "";
                    } else {
                        error = "No se encontro un servicio especifico para el LA ingresado, se usa servicio generico para el operdaor";
                    }
                }
            } catch (Exception e) {
                logger.error("Error al recuperar servicio para operador=" + opString + ", la=" + la);
                error = "Ocurrio un error al intentar de recuperar el servicio adecuado de envio.";
                servicio = "";
            }

            resultado = servicio;
        } else if (req.equals(REQ_ELIMINARLISTANEGRA)) {
            int broadcastId = RequestUtil.getParameter(request, "broadcast", -1);
            try {
                Broadcast broadcast = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);
                BroadcastsManagerWrapper.updateBroadcastListaNegraArchivo(broadcast, null, bcm);
                id = 1;
            } catch (Exception e) {
                logger.error("Error en EdicionAjax para  al eliminar lista negra", e);
                id = -1;
                error = "Ha ocurrido un error al procesar la peticion: " + e.getClass().getName() + "::" + e.getMessage();
            }
        } else if (req.equals(REQ_FECHA)) {
            //retornamos
            //id, error, resultado
            String fechaInput = RequestUtil.getParameter(request, "fecha", "");
            Operador operador = Operador.valueOf(RequestUtil.getParameter(request, "operador", null));
            try {
                resultado = WebUtil.tranformaUTCaTimeZone(fechaInput, "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mm z", operador);
                id = 1;
            } catch (ParseException e) {
                resultado = "Error";
                error = "No se pudo tranformar la fecha " + fechaInput + " a " + Configuracion.get(operador.getPais() + ".TimeZone");
            }
        } else if (req.equals(REQ_FECHA_UTC)) {
            //retornamos
            //id, error, resultado
            String fechaInput = RequestUtil.getParameter(request, "fecha", "");
            Operador operador = Operador.valueOf(RequestUtil.getParameter(request, "operador", null));
            logger.info("req "+fechaInput+ " "+operador);
            try {
                resultado = WebUtil.tranformaTimeZoneaUTC(fechaInput, "yyyy-MM-dd HH:mm", operador, "yyyy-MM-dd HH:mm z");
                id = 1;
            } catch (ParseException e) {
                resultado = "Error";
                error = "No se pudo tranformar la fecha " + fechaInput + " a " + Configuracion.get(operador.getPais() + ".TimeZone");
            }
        }

        //response.getOutputStream().print(" { id: " + id + " , error= \"" + error + "\"}");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        jsonObject.put("resultado", resultado);
        jsonObject.put("error", error);
        logger.info(jsonObject.toString());
        response.getOutputStream().print(jsonObject.toString());
    }

    private String getServicioFromBD(Operador operador, String la, ElementoMensaje elementoMensaje) throws SQLException {

        logger.debug("En metodo getServicioFromBD | operador : " + operador.name() + " | la : " + la + " | elemento Mensaje : " + elementoMensaje.getTipo().name());
        Connection conexion_gmmadmin = null;
        ArrayList listaServiciosCandidatos = new ArrayList();

        try {
            conexion_gmmadmin = DBManager.getConnection("bd_gmmadmin");
            logger.debug("Conexion al gmm_admin establecida");
        } catch (Exception e) {
            logger.error("Error | No se establecio conexion a la BD de gmm_admin : " + e);
        }

        String sql = "SELECT servicio from servicioprecios  where operador = ? and servicio like 'br%' and la = ? and tipo = ? and estado = 1 and precio = '0'";

        String tipo_mensaje = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conexion_gmmadmin.prepareStatement(sql);
            ps.setInt(1, operador.getIdGMM());
            ps.setString(2, la.trim());
            ps.setString(3, tipo_mensaje);

            rs = ps.executeQuery();

            String servicio = "";
            while (rs.next()) {
                servicio = rs.getString("servicio");
                logger.debug("Servicio obtenido : " + servicio);
                listaServiciosCandidatos.add(servicio.trim());
            }
        } catch (Exception ex) {
            logger.error("Error al obtener los servicios del gmm_admin : " + ex);
        } finally {
            WebUtil.closeStatements(rs, ps);
        }

        try {
            conexion_gmmadmin.close();
            logger.debug("Conexiones a gmm_admin cerrada exitosamente");
        } catch (Exception e) {
            logger.error("Error | No se cerro conexion a la BD de gmm_admin : " + e);
        }

        return getMejorCandidato(listaServiciosCandidatos);
    }

    private String getMejorCandidato(ArrayList<String> listaServiciosCandidatos) {
        String servicio_cantidato = "";

        String maquina_instalada = Configuracion.get("contenedorBroadcast.id");
        logger.debug("Se obtuvo la cadena de la maquina instalada : " + maquina_instalada);

        String pattern_broadcast_search = "";

        if (maquina_instalada.contains("Apl12")) {
            pattern_broadcast_search = "broadcast_";
        }

        if (maquina_instalada.contains("xen13")) {
            pattern_broadcast_search = "brdcast";
        }

        for (String servicio : listaServiciosCandidatos) {
            //el prefijo broadcast_ implica que tiene la nueva nomenclaruta y que esta en BCM1
            //el sufijo SMPP es para que se vaya por la conexion rapida de cualqueir operador
            logger.debug("Analizando al servicio candidato : " + servicio);
            if (servicio.startsWith(pattern_broadcast_search) && servicio.contains("SMPP")) {
                return servicio;
            } else if (servicio.startsWith(pattern_broadcast_search)) {
                return servicio;
            }
        }
        return servicio_cantidato;
    }

}
