package com.movixla.broadcastmanager.servlet;

import com.movix.shared.util.Configuracion;
import com.movix.shared.util.RequestUtil;
import com.movix.shared.util.Util;
import com.movixla.broadcastmanager.base.AppWebBaseServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: rparedes
 * Date: Mar 8, 2011
 * Time: 2:48:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class VerRecursoWebServlet extends AppWebBaseServlet {
    private static final int RECURSO_ELEMENTO_MENSAJE_IMAGEN = 1;

    /*
     * doGet del servicio http
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("doGet->Parametros: " + Util.parameters2String(request));
        int tipoRecurso = -1;
        try {
            tipoRecurso = RequestUtil.getParameter(request, "tipo", -1);
            switch (tipoRecurso) {
                case RECURSO_ELEMENTO_MENSAJE_IMAGEN: {
                    verElementoMensajeImagen(request, response);
                    break;
                }
            }
        } catch (Exception exc) {
            logger.error("doGet.Excepcion->" + exc.getMessage(), exc);
        }
    }

    /*
     * Ver elemento de un mensaje tipo imagen
     */
    private void verElementoMensajeImagen(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("verElementoMensajeImagen->Inicio");
        String url = null;
        String filePathBase = null;
        File archivoImagen = null;
        boolean existeArchivo = false;
        try {
            String bcm = RequestUtil.getParameter(request, "bcm", "");
            filePathBase = Configuracion.get(bcm + ".path_bases");
            url = RequestUtil.getParameter(request, "url", null);
            logger.info("filePathBase:" + filePathBase + " | url:" + url);
            archivoImagen = new File(filePathBase + url);
            existeArchivo = archivoImagen.exists();
            logger.info("existeArchivo:" + existeArchivo);
            if (existeArchivo) {
                enviaBytesArchivo(response.getOutputStream(), archivoImagen);
            }

        } catch (Exception exc) {
            logger.error("verElementoMensajeImagen.Excepcion->" + exc.getMessage(), exc);
        }
        logger.info("verElementoMensajeImagen->Fin");
    }

    /*
     * envia los bytes del archivo pasado de parametro
     */
    private void enviaBytesArchivo(OutputStream out, File archivo) throws IOException {
        byte buffer[];
        int porleer, leidos;
        FileInputStream in = null;
        BufferedInputStream bin = null;
        try {
            porleer = (int) archivo.length();
            buffer = new byte[60000];
            in = new FileInputStream(archivo);
            bin = new BufferedInputStream(in);
            while (porleer > 0) {
                leidos = bin.read(buffer);
                if (leidos > 0) {
                    out.write(buffer, 0, leidos);
                    porleer -= leidos;
                } else if (leidos < 0) {
                    throw new IOException("Se encontro el fin del archivo de manera inesperada");
                }
            }
        }
        catch (Exception exc) {
            logger.info("enviaBytesArchivo.Excepcion->" + exc.getMessage(), exc);
        }
    }
}
