package com.movixla.broadcastmanager.servlet;

import com.movix.shared.util.RequestUtil;
import com.movix.shared.util.Util;
import com.movix.shared.wap.WapUtil;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcastmanager.aplicacion.domobj.Perfil;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.WebUtil;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.BroadcastsManagerWrapper;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.PerfilManagerWrapper;
import com.movixla.broadcastmanager.base.AppWebBaseServlet;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Andres
 * Date: 04-jun-2009
 * Time: 15:40:08
 */
public class ListaServlet extends AppWebBaseServlet {
    private static final String ACCION_DETENER = "detener";
    private static final String ACCION_REANUDAR = "reanudar";
    private static final String ACCION_CANCELAR = "cancelar";
    private static final String ACCION_ELIMINAR = "eliminar";
    private static final SimpleDateFormat f_yyyyMMdd_HHmm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    
    static{
        
    }

    /*
     * Doget del servicio http
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<String, Object> varValues = new HashMap<String, Object>(7);
        
        Usuario usuario = WebUtil.getUsuarioSesion(request);
        varValues.put("usuario", usuario);
        logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));
        
        Perfil perfil = null;
        if (usuario == null) {
            //lo enviamos a la pagina de login
            response.sendRedirect("index.jhtml");
            return;
        }
        try{
            perfil = PerfilManagerWrapper.getPerfilForUser(usuario,"bcm5");
        }catch (Exception e){
            logger.error(e);
        }

        String filtrar = RequestUtil.getParameter(request, "filtar", "", false);
        filtrar = request.getParameter("filtrar");

        boolean incluirCancelados = RequestUtil.getParameter(request, "filtrar_cancelado", "off").equals("on");
        boolean incluirTerminados = RequestUtil.getParameter(request, "filtrar_terminado", "off").equals("on");
        //si filtrar es nulo, sacamos los valores de las cookies.
        if (filtrar == null) {
            filtrar = WebUtil.getCookieValueDecoded(request, "filtrar_" + usuario.getId());
            if (filtrar != null) {
                incluirCancelados = WebUtil.getCookieValueDecoded(request, "filtrar_cancelado_" + usuario.getId()).equals("on");
                incluirTerminados = WebUtil.getCookieValueDecoded(request, "filtrar_terminado_" + usuario.getId()).equals("on");
            } else {
                filtrar = "";
            }
        }
        filtrar = filtrar.trim().toLowerCase();
        //guardamos los valores en las cookies.
        response.addCookie(WebUtil.getNewCookieEncoded("filtrar_" + usuario.getId(), filtrar));
        response.addCookie(WebUtil.getNewCookieEncoded("filtrar_cancelado_" + usuario.getId(), incluirCancelados ? "on" : "off"));
        response.addCookie(WebUtil.getNewCookieEncoded("filtrar_terminado_" + usuario.getId(), incluirTerminados ? "on" : "off"));


        Map<String, Broadcast> broadcastsFiltrados = new HashMap<String, Broadcast>();
        Map<String, Broadcast> broadcasts = new HashMap<String, Broadcast>();
        HashMap mapResultado = new LinkedHashMap();
        HashMap mapResultadoUpDown = new LinkedHashMap();
        try {
            logger.info("Carga lista de broadcasts");
            broadcasts = BroadcastsManagerWrapper.getAllBroadcastsNoEliminados(usuario);
            logger.info("AllBroadcastsNoEliminados::" + (broadcasts != null ? broadcasts.size() : 0));
            int countNoActivos = 0;

            Iterator it = broadcasts.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry broadcast = (Map.Entry) it.next();
                String bcId = (String) broadcast.getKey();
                Broadcast bc = (Broadcast) broadcast.getValue();

                String textoBroadacst = getStringFiltrarBroadcast(bcId, bc);
                if (bc.getEstado().equals(Broadcast.Estado.CANCELADO) && incluirCancelados && textoBroadacst.contains(filtrar)) {
                    if (countNoActivos < 100) broadcastsFiltrados.put(bcId, bc);
                    countNoActivos++;
                } else if (bc.getEstado().equals(Broadcast.Estado.TERMINADO) && incluirTerminados && textoBroadacst.contains(filtrar)) {
                    if (countNoActivos < 100) broadcastsFiltrados.put(bcId, bc);
                    countNoActivos++;
                } else if (!WebUtil.isInactivo(bc) && textoBroadacst.contains(filtrar)) { //si no e terminado, cancelado o eliminado
                    broadcastsFiltrados.put(bcId, bc);
                }

            }


            Map mapOrdenado = new TreeMap(broadcastsFiltrados);
            logger.info("*******************broadcastsFiltrados => " + broadcastsFiltrados.size());
            Set ref = mapOrdenado.keySet();
            Iterator itrtr = ref.iterator();
            while (itrtr.hasNext()) {
                logger.info((String) itrtr.next());
            }


            List misMapKeys = new ArrayList(broadcastsFiltrados.keySet());
            List misMapValues = new ArrayList(broadcastsFiltrados.values());
            TreeSet conjuntoOrdenado = new TreeSet(misMapKeys);
            Object[] arrayOrdenado = conjuntoOrdenado.toArray();
            Arrays.sort(arrayOrdenado, Collections.reverseOrder());
            int size = arrayOrdenado.length;
            logger.info("=>**************************" + misMapKeys + "*****");
            for (int i = 0; i < size; i++) {
                mapResultado.put(arrayOrdenado[i], misMapValues.get(misMapKeys.indexOf(arrayOrdenado[i])));
            }

            Iterator it1 = mapResultado.entrySet().iterator();
            logger.info("1**************************");
            while (it1.hasNext()) {
                Map.Entry e = (Map.Entry) it1.next();
                logger.info(e.getKey());
            }

        } catch (SQLException e) {
            logger.error("Error al recuperar broadcasts", e);
        }


        varValues.put("broadcasts", mapResultado);
        varValues.put("filtrar", filtrar);
        varValues.put("filtrar_cancelado", incluirCancelados);
        varValues.put("filtrar_terminado", incluirTerminados);
        varValues.put("perfil", perfil);
        
        showTemplate(request, response, varValues);
    }

    /**
     * Maneja las acciones efectadas en la lista de broadcasts.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accion = RequestUtil.getParameter(request, "accion", "").trim();
        int broadcastId = RequestUtil.getParameter(request, "broadcast", -1);
        String bcm = "bcm5";
        Usuario usuario = WebUtil.getUsuarioSesion(request);
        logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));

        int resultado = 0;
        String error = "";

        if (usuario == null) {

            error = "Sesion Expirada.";
        }

        try {
            Broadcast broadcast = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);
            if (broadcast != null) {
                if (accion.equals(ACCION_DETENER)) {
                    if (broadcast.getEstado().equals(Broadcast.Estado.EN_PROCESO)) {
                        BroadcastsManagerWrapper.updateBroadcastEstado(broadcast, Broadcast.Estado.DETENIDO, "Fecha: " + f_yyyyMMdd_HHmm.format(new Date()), bcm);
                        resultado = 1;
                    } else {
                        error = "Solo broadcasts EN_PROCESO puede ser detenido.";
                    }
                } else if (accion.equals(ACCION_REANUDAR)) {
                    if (broadcast.getEstado().equals(Broadcast.Estado.DETENIDO)) {
                        BroadcastsManagerWrapper.updateBroadcastEstado(broadcast, Broadcast.Estado.EN_PROCESO, "", bcm);
                        resultado = 1;
                    } else {
                        error = "Solo un broadcast DETENIDO puede ser reanudado.";
                    }
                } else if (accion.equals(ACCION_CANCELAR)) {
                    if (!WebUtil.isInactivo(broadcast)) {
                        BroadcastsManagerWrapper.updateBroadcastEstado(broadcast, Broadcast.Estado.CANCELADO, "Fecha: " + f_yyyyMMdd_HHmm.format(new Date()), bcm);
                        resultado = 1;
                    } else {
                        error = "Solo un broadcast activo puede ser reanudado.";
                    }
                } else if (accion.equals(ACCION_ELIMINAR)) {
                    if (WebUtil.isInactivo(broadcast)) {
                        BroadcastsManagerWrapper.updateBroadcastEstado(broadcast, Broadcast.Estado.ELIMINADO, "Fecha: " + f_yyyyMMdd_HHmm.format(new Date()), bcm);
                        resultado = 1;
                    } else {
                        error = "Solo un broadcast inactivo puede ser reanudados.";
                    }
                } else {
                    error = "Accion '" + accion + "' no es valida.";
                }
            } else {
                error = "No se encontro broadcast id " + broadcastId;
            }
        } catch (SQLException e) {
            logger.error("No se pudo recuperar broadcast", e);
            error = "Error al realizar la accion " + e.getClass() + ": " + e.getMessage();
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("resultado", resultado);
        jsonObject.put("error", error);

        logger.info(jsonObject.toString());
        response.getOutputStream().print(jsonObject.toString());


    }

    public String getStringFiltrarBroadcast(String bcm, Broadcast b) {
        return (b.getId() + "" + b.getNombre().trim() + "" + b.getOperador() + "" + f_yyyyMMdd_HHmm.format(b.getFechaInicio()) + "" + f_yyyyMMdd_HHmm.format(b.getFechaFin()) + "" + b.getEstado() + "" + bcm.split("-")[0] + "").trim().toLowerCase();
    }

}
