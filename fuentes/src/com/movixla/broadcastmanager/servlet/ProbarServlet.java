package com.movixla.broadcastmanager.servlet;

import com.movix.shared.util.RequestUtil;
import com.movix.shared.util.Util;
import com.movix.shared.wap.WapUtil;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.ClienteBroadcastManager;
import com.movixla.broadcastmanager.aplicacion.WebUtil;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.BroadcastsManagerWrapper;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.MensajesManagerWrapper;
import com.movixla.broadcastmanager.base.AppWebBaseServlet;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Andres
 * Date: 04-ago-2009
 * Time: 10:24:46
 */
public class ProbarServlet extends AppWebBaseServlet {


    /*
     * doGet del servicio http.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<String, Object> varValues = new HashMap<String, Object>(4);

        int broadcastId = RequestUtil.getParameter(request, "broadcast_id", -1);
        String pruebas = RequestUtil.getParameter(request, "pruebas", null);
        String bcm = "bcm5";

        boolean inicio = false;

        Usuario usuario = WebUtil.getUsuarioSesion(request);
        logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));
        if (usuario == null) {
            //lo enviamos a la pagina de login
            response.sendRedirect("index.jhtml");
            return;
        }


        if (pruebas == null) {
            inicio = true;

            pruebas = WebUtil.getCookieValueDecoded(request, "testMessageBroadcast_" + broadcastId);
            if (pruebas == null) {
                pruebas = "";
            }
        }


        Broadcast broadcast = null;
        Mensaje mensaje = null;
        try {
            broadcast = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);
            mensaje = MensajesManagerWrapper.getMensajeByBroadcast(broadcast, bcm);
        } catch (SQLException e) {
            logger.error("Error al recuperar broadcasts", e);
            return;
        }

        String error = (String) request.getAttribute("errores");

        varValues.put("broadcast", broadcast);
        varValues.put("mensaje", mensaje);
        varValues.put("pruebas", pruebas);
        varValues.put("error", error);
        varValues.put("inicio", inicio);
        varValues.put("bcm", bcm);

        showTemplate(request, response, varValues);
    }

    /*
     * doPost del servicio http.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int broadcastId = RequestUtil.getParameter(request, "broadcast_id", -1);
        String pruebas = RequestUtil.getParameter(request, "pruebas", "").trim();
        String bcm = "bcm5";


        response.addCookie(WebUtil.getNewCookieEncoded("testMessageBroadcast_" + broadcastId, pruebas));

        Usuario usuario = WebUtil.getUsuarioSesion(request);
        logger.info(usuario + " | parametros = " + Util.parameters2String(request) + " | ip = " + WapUtil.getIp(request));

        if (usuario == null) {
            //lo enviamos a la pagina de login
            response.sendRedirect("index.jhtml");
            return;
        }

        String error = "";
        try {
            Broadcast broadcast = BroadcastsManagerWrapper.getBroadcastById(broadcastId, bcm);
            if (broadcast != null) {
                String[] lineas = pruebas.split("\n");
                List<String> moviles = new ArrayList<String>(lineas.length);
                for (String string : lineas) {
                    moviles.add(string);
                }
                try {
                    logger.info("PROBO ---- ClienteBroadcastManager.getInstance(" + bcm + ").probar(" + broadcast + ", " + moviles + ")");
                    ClienteBroadcastManager.getInstance(bcm).probar(broadcast, moviles, bcm);
                } catch (Exception e) {
                    error = "Error al realizar la prueba " + e.getClass() + ": " + e.getMessage();
                }
            } else {
                error = "No se encontro broadcast id " + broadcastId;
            }
        } catch (SQLException e) {
            logger.error("No se pudo recuperar broadcast", e);
            error = "Error al realizar la accion " + e.getClass() + ": " + e.getMessage();
        }

        request.setAttribute("errores", error);
        doGet(request, response);
    }


    public static void main(String[] args) throws IOException {
        String lala = "56997164115;com\n" +
                "56997164115;NET";
        String encoded = new BASE64Encoder().encode(lala.getBytes());
        String decoded = new String(new BASE64Decoder().decodeBuffer(encoded));
        System.out.println("lala = '" + lala + "'");
        System.out.println("encoded = '" + encoded + "'");
        System.out.println("decoded = '" + decoded + "'");

    }
}
