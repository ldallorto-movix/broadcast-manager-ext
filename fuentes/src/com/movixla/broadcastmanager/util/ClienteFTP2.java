package com.movixla.broadcastmanager.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class ClienteFTP2 {
    
    private static final Logger logger = LoggerFactory.getLogger(ClienteFTP2.class);
    

	private static final String LOCAL_TMP_PATH = "/srv/produccion/data/broadcastManager/tmp/";
	private static final String FTP_PROCESSED_FOLDER = "backup";
	private static final String SFTP_CHANNEL = "sftp";

	JSch jsch = null;
	Session session = null;
	ChannelSftp sftp = null;

	/**
	 * Crea un cliente FTP
	 * 
	 * @param username
	 * @param server
	 * @param port
	 * @param password
	 * @throws JSchException
	 * @throws SocketException
	 * @throws IOException
	 */
	public ClienteFTP2(String username, String server, int port, String password) throws JSchException {
		jsch = new JSch();
		session = null;
		sftp = null;

		session = jsch.getSession(username, server, port);
		session.setUserInfo(new SFTPUserInfo(password, null));
		session.setPassword(password);
		session.connect();
	}

	/**
	 * Permite bajar un archivo desde el FTP. Si no existe, devolvera null y tal
	 * vez aparezca un warn en el log. Una vez que baja el archivo, en el ftp lo
	 * mueve a la carpeta backups
	 * 
	 * @param filename
	 * @param remotePath
	 * @return
	 */
	public String downloadFile(String filename, String remotePath) {
	    logger.info("calling downloadFile(), filename:{}, remotePath:{}", filename, remotePath);

		try {

			sftp = (ChannelSftp) session.openChannel(SFTP_CHANNEL);
			sftp.connect();

			// Bajar el archivo remoto
			sftp.get(remotePath + "/" + filename, LOCAL_TMP_PATH + filename);

			// mover a otro lado el archivo remoto
//			renameFile(remotePath,filename);

			return LOCAL_TMP_PATH + filename;
		} catch (SftpException e) {
			logger.error("Ocurrio un error en downloadFile.. SftpException", e);
			//e.printStackTrace();
		} catch (JSchException e) {
			logger.error("Ocurrio un error en downloadFile..JSchException", e);
			//e.printStackTrace();
		} finally {
			if (null != sftp) {
				sftp.exit();
				sftp.disconnect();
			}
		}

		return null;
	}
	
	public String downloadFile(String filename, String remotePath, String newFileName) {
        logger.info("calling downloadFile(), filename:{}, remotePath:{}", filename, remotePath);

        try {

            sftp = (ChannelSftp) session.openChannel(SFTP_CHANNEL);
            sftp.connect();

            // Bajar el archivo remoto
            sftp.get(remotePath + "/" + filename, LOCAL_TMP_PATH + newFileName);

            // mover a otro lado el archivo remoto
//          renameFile(remotePath,filename);

            return LOCAL_TMP_PATH + newFileName;
        } catch (SftpException e) {
            logger.error("Ocurrio un error en downloadFile.. SftpException", e);
            //e.printStackTrace();
        } catch (JSchException e) {
            logger.error("Ocurrio un error en downloadFile..JSchException", e);
            //e.printStackTrace();
        } finally {
            if (null != sftp) {
                sftp.exit();
                sftp.disconnect();
            }
        }

        return null;
    }
	
	public void renameFileDuplicated(String filename, String remotePath) {

		try {

			sftp = (ChannelSftp) session.openChannel(SFTP_CHANNEL);
			sftp.connect();

			// mover a otro lado el archivo remoto
			renameFile(remotePath,filename);

		} catch (SftpException e) {
			logger.error("Ocurrio un error al renameFile un archivo..", e.getMessage());
			//e.printStackTrace();
		} catch (JSchException e) {
			logger.error("Ocurrio un error al renameFile un archivo..", e.getMessage());
			//e.printStackTrace();
		} finally {
			if (null != sftp) {
				sftp.exit();
				sftp.disconnect();
			}
		}

	}
	
	private void renameFile(String remotePath,String filename) throws SftpException{
		String strDate = getFechaActual();
		sftp.rename(remotePath + "/" + filename, remotePath + "/" + FTP_PROCESSED_FOLDER + "/" + filename + "_" + strDate);
	}

	/**
	 * busca un archivo en el ftp, en el directorio indicado Si no encuentra,
	 * devuelve null.
	 * 
	 * @param prefix
	 * @param remotePath
	 * @return
	 */
	public String searchFileByPrefix(String prefix, String remotePath) {
		Vector<ChannelSftp.LsEntry> sftpFileList = null;
		String filename = null;
		try {

			sftp = (ChannelSftp) session.openChannel(SFTP_CHANNEL);
			sftp.connect();
			sftpFileList = sftp.ls(remotePath);
			for (ChannelSftp.LsEntry lsEntry : sftpFileList) {
				if (!lsEntry.getAttrs().isDir()) {
					logger.debug(lsEntry.getFilename() + " vs " + prefix + ": " + lsEntry.getFilename().startsWith(prefix));
					if (lsEntry.getFilename().startsWith(prefix)) {
						filename = lsEntry.getFilename();
						break;
					}
				}
			}

		} catch (SftpException e) {
			logger.error("Ocurrio un error al buscar un archivo..", e.getMessage());
			System.out.println(e.getMessage());
		} catch (JSchException e) {
			logger.error("Ocurrio un error al buscar un archivo..", e.getMessage());
			System.out.println(e.getMessage());
		} finally {
			if (null != sftp) {
				sftp.exit();
				sftp.disconnect();
			}
		}

		return filename;
	}
	
	public Boolean checkDir(String remotePath){
        try {

            sftp = (ChannelSftp) session.openChannel(SFTP_CHANNEL);
            sftp.connect();
            sftp.cd(remotePath);
            
        } catch (SftpException e) {
        logger.error("No existe el directorio..", e.getMessage());
        return Boolean.FALSE;
        }
        catch (JSchException e) {
            logger.error("Ocurrio un error al buscar el directorio..", e.getMessage());
        }finally {
            if (null != sftp) {
                sftp.exit();
                sftp.disconnect();
            }
        }
        
        
            return Boolean.TRUE;
        }
	
	public Boolean checkFile(String fileName, String remotePath) {
	    Boolean result = Boolean.FALSE;
	    
        Vector<ChannelSftp.LsEntry> sftpFileList = null;
        String filename = null;
        try {

            sftp = (ChannelSftp) session.openChannel(SFTP_CHANNEL);
            sftp.connect();
            
            sftpFileList = sftp.ls(remotePath);
            for (ChannelSftp.LsEntry lsEntry : sftpFileList) {
                if (!lsEntry.getAttrs().isDir()) {
                    logger.info(lsEntry.getFilename() + " vs " + fileName + ": " + lsEntry.getFilename().equalsIgnoreCase(fileName));
                    if (lsEntry.getFilename().equalsIgnoreCase(fileName)) {
                        result = Boolean.TRUE;
                    }
                }
            }

        } catch (SftpException e) {
            logger.error("Ocurrio un error en checkFile..", e.getMessage());
            System.out.println(e.getMessage());
        } catch (JSchException e) {
            logger.error("Ocurrio un error en checkFile..", e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            if (null != sftp) {
                sftp.exit();
                sftp.disconnect();
            }
        }

        return result;
    }
	
	public int executeRemoteCommand(String command){
	    int exitStatus = -100;
	    
	            try{
	                Channel channel=session.openChannel("exec");
	                ((ChannelExec)channel).setCommand(command);
	                channel.setInputStream(null);
	                ((ChannelExec)channel).setErrStream(System.err);
	                InputStream in=channel.getInputStream();
	                channel.connect();
	                byte[] tmp=new byte[1024];
	                while(true){
	                  while(in.available()>0){
	                    int i=in.read(tmp, 0, 1024);
	                    if(i<0)break;
	                    logger.info("{}", new String(tmp, 0, i));
	                  }
	                  if(channel.isClosed()){
	                    logger.info("exit-status: {}", channel.getExitStatus());
	                    exitStatus = channel.getExitStatus();
	                    break;
	                  }
	                  try{Thread.sleep(1000);}catch(Exception ee){}
	                }

	            }catch(Exception e){
	                logger.error("error",e);
	            }
	            
	    return exitStatus;    

	}

	/**
	 * Cierra la conexion al FTP, con esto se muere la instancia y ya no sirve
	 * para bajar mas cosas.
	 */
	public void closeConnection() {
		try {
			if (null != session && session.isConnected()) {
				session.disconnect();
			}
		} catch (Exception exc) {
			logger.error("Exception",exc);
		}
		session = null;
	}

	@Override
	protected void finalize() throws Throwable {
		closeConnection();
		super.finalize();
	}

	/**
	 * Obtiene la fecha actual en formato.
	 * 
	 * @return
	 */
	private String getFechaActual() {
		Date d = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(d);
	}
	
	public static void main (String[] arg) throws Exception{
		ClienteFTP2 clienteFTP = new ClienteFTP2("produccion", "192.168.1.58", 22, "$Smgoon10");
	    
		String command1="ls -ltr /srv/produccion/log/";
		clienteFTP.executeRemoteCommand(command1);
		
		clienteFTP.closeConnection();
		
	}

}
