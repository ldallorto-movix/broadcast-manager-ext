package com.movixla.broadcastmanager.util;

import com.jcraft.jsch.UserInfo;

/**
 * Created by IntelliJ IDEA. User: rparedes Date: Oct 27, 2010 Time: 4:04:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class SFTPUserInfo implements UserInfo {
	private String password;
	private String passPhrase;

	public SFTPUserInfo(String password, String passPhrase) {
		this.password = password;
		this.passPhrase = passPhrase;
	}

	public String getPassphrase() {
		return passPhrase;
	}

	public String getPassword() {
		return password;
	}

	public boolean promptPassphrase(String arg0) {
		return true;
	}

	public boolean promptPassword(String arg0) {
		return false;
	}

	public boolean promptYesNo(String arg0) {
		return true;
	}

	public void showMessage(String arg0) {

	}
}
