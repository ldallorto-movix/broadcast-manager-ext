package com.movixla.broadcastmanager.aplicacion.bo;

import com.movixla.broadcastmanager.aplicacion.domobj.BCMConexionDO;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: rparedes
 * Date: Dec 9, 2010
 * Time: 9:58:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class BCMConexionBO {
    private static List<BCMConexionDO> conexions;
    private static Logger logger = Logger.getLogger(BCMConexionBO.class);

    /*
     * Obtiene la conexion a la base de datos
     */
    public static List<BCMConexionDO> getConexiones() {
        if (conexions == null) {
            conexions = new ArrayList<BCMConexionDO>();
            conexions.add(new BCMConexionDO("bcm5.bd", "broadcastmanagerF5"));

        }
        return conexions;
    }

    public static BCMConexionDO getConexionPorId(String id) {
        getConexiones();
        for (BCMConexionDO conexion : conexions) {
            if (conexion.getId().equals(id)) return conexion;
        }
        return null;
    }
}