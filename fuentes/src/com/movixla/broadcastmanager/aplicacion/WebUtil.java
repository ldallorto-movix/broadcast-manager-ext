package com.movixla.broadcastmanager.aplicacion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.movix.shared.Operador;
import com.movix.shared.Pais;
import com.movix.shared.util.Configuracion;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.ElementoMensaje;
import com.movixla.broadcast.shared.Notificacion;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;

/**
 * Created by Andres
 * Date: 22-may-2009
 * Time: 16:34:54
 */
public class WebUtil {

    protected static final Logger logger = Logger.getLogger(WebUtil.class);

    private static final String USUARIO_SESION = "usuario";

    static HashMap<Pais, Pattern> patterns;

    static {
        patterns = new HashMap<Pais, Pattern>();
        patterns.put(Pais.CHILE, Pattern.compile("^[0-9]{11}(;.*)*"));
        patterns.put(Pais.PERU, Pattern.compile("^[0-9]{12,13}(;.*)*"));
        patterns.put(Pais.COLOMBIA, Pattern.compile("^[0-9]{12}(;.*)*"));
        patterns.put(Pais.ECUADOR, Pattern.compile("^[0-9]{11}(;.*)*"));
        patterns.put(Pais.ARGENTINA, Pattern.compile("^[0-9]{12}(;.*)*"));
        patterns.put(Pais.PANAMA, Pattern.compile("^[0-9]{11}(;.*)*"));

        patterns.put(Pais.DESCONOCIDO, Pattern.compile("^[0-9]{11,13}(;.*)*"));
    }

    public static void setUsuarioSesion(HttpServletRequest request, Usuario usuario) {
        HttpSession session = request.getSession(true);
        session.setAttribute(USUARIO_SESION, usuario);
    }

    public static Usuario getUsuarioSesion(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        return (Usuario) session.getAttribute(USUARIO_SESION);
    }

    public static Date getFechaInicioDefault() {
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    public static Date getFechaFinDefault() {
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.YEAR, 1);
        return calendar.getTime();
    }

    public static Mensaje getMensajeDummy(Broadcast broadcast) {
        return new Mensaje(0, broadcast.getId(), Mensaje.Tipo.SMS, "", "", "", "", "", 0, Mensaje.Estado.NUEVO, "", "");
    }

    public static ElementoMensaje getElementoMensajeTextoDummy(Mensaje mensaje) {
        return new ElementoMensaje(0, mensaje.getId(), ElementoMensaje.Tipo.TEXT, "", null, 3000, ElementoMensaje.Estado.NUEVO);
    }

    public static ElementoMensaje getElementoMensajeImagenDummy(Mensaje mensaje) {
        return new ElementoMensaje(0, mensaje.getId(), ElementoMensaje.Tipo.IMG, null, null, 3000, ElementoMensaje.Estado.NUEVO);
    }

    public static Notificacion getNotificacionDummy(Broadcast broadcast) {
        return new Notificacion(0, broadcast.getId(), Notificacion.Tipo.EMAIL, "", 1000, Notificacion.Perfil.INTERNO,
                Notificacion.Estado.NUEVO);
    }

    public static String checkBase(File file) throws IOException {
        return (checkBase(file, Operador.DIGICEL_PANAMA));
    }

    public static String checkBase(File file, Operador operador) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        Pattern pattern = patterns.get(operador.getPais());
        while ((line = reader.readLine()) != null) {
            if (line.trim().equals(""))
                continue;
            if (!pattern.matcher(line).matches()) {

                return line;
            }
        }
        reader.close();
        return "";
    }

    public static boolean isInactivo(Broadcast broadcast) {
        return broadcast.getEstado().equals(Broadcast.Estado.CANCELADO)
                || broadcast.getEstado().equals(Broadcast.Estado.TERMINADO)
                || broadcast.getEstado().equals(Broadcast.Estado.ELIMINADO);
    }

    public static boolean isEnProceso(Broadcast broadcast) {
        return broadcast.getEstado().equals(Broadcast.Estado.EN_PROCESO)
                || broadcast.getEstado().equals(Broadcast.Estado.DETENIDO);
    }
    
    public static String tranformaTimeZoneaUTC(String fecha, String formatoEntrada, Operador op, String formatoSalida)
            throws ParseException {
        return tranformaTimeZoneaUTC(fecha, formatoEntrada, op.getPais(), formatoSalida);
    }

    public static String tranformaUTCaTimeZone(String fecha, String formatoEntrada, String formatoSalida, Operador op)
            throws ParseException {
        return tranformaUTCaTimeZone(fecha, formatoEntrada, formatoSalida, op.getPais());
    }

    public static String tranformaUTCaTimeZone(String fecha, String formatoEntrada, String formatoSalida, Pais p)
            throws ParseException {
        // TimeZone tz = TimeZone.getTimeZone(Configuracion.get(p + ".TimeZone"));

        TimeZone tz = p.getTimeZone();
        if (Pais.CHILE.equals(p)) {
            boolean clDst = checkForBooleanProperty("bcm.dst.chile");
            logger.info("checkForBooleanProperty:" + clDst);
            String timeZoneId = (clDst == true) ? "GMT-3" : "GMT-4";
            tz = TimeZone.getTimeZone(timeZoneId);
        }

        String tranformaUTCaTimeZone = tranformaUTCaTimeZone(fecha, formatoEntrada, formatoSalida, tz);
        return replaceExplicitTimeZoneWithName(p, tranformaUTCaTimeZone);

    }
    
    public static String tranformaTimeZoneaUTC(String fecha, String formatoEntrada, Pais p,String formatoSalida)
            throws ParseException {
  
        TimeZone tz = p.getTimeZone();
        if (Pais.CHILE.equals(p)) {
            boolean clDst = checkForBooleanProperty("bcm.dst.chile");
            logger.info("checkForBooleanProperty:" + clDst);
            String timeZoneId = (clDst == true) ? "GMT-3" : "GMT-4";
            tz = TimeZone.getTimeZone(timeZoneId);
        }

        String tranformaTimeZoneaUTC = tranformaTimeZoneaUTC(fecha, formatoEntrada,tz, formatoSalida);
        return tranformaTimeZoneaUTC;

    }

    private static boolean checkForBooleanProperty(String key) {
        String value = Configuracion.get(key);

        logger.info("KeyValue" + value);
        if (StringUtils.isBlank(value)) {
            return false;
        }

        return "true".equalsIgnoreCase(value.trim());
    }

    private static String replaceExplicitTimeZoneWithName(Pais p, String utcATimeZone) {

        if (Pais.CHILE.equals(p)) {
            utcATimeZone = utcATimeZone.replace("GMT-03:00", "CLT");
            utcATimeZone = utcATimeZone.replace("GMT-04:00", "CLT");
        }
        return utcATimeZone;
    }

    // reescribe un string que representa una fecha en utc a un nuevo timezone.
    public static String tranformaUTCaTimeZone(String fecha, String formatoEntrada, String formatoSalida, TimeZone tz)
            throws ParseException {
        // parseamos la fecha en utc
        SimpleDateFormat formatUTC = new SimpleDateFormat(formatoEntrada);
        formatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = formatUTC.parse(fecha);

        // escribimos la fecha segun el nuevo timezone
        SimpleDateFormat formatLocal = new SimpleDateFormat(formatoSalida);
        formatLocal.setTimeZone(tz);
        return formatLocal.format(date);
    }
    
    public static String tranformaTimeZoneaUTC(String fecha, String formatoEntrada,TimeZone tz, String formatoSalida)
            throws ParseException {

       // escribimos la fecha segun el nuevo timezone
        SimpleDateFormat formatLocal = new SimpleDateFormat(formatoEntrada);
        formatLocal.setTimeZone(tz);
        Date date = formatLocal.parse(fecha);
        // parseamos la fecha en utc
        SimpleDateFormat formatUTC = new SimpleDateFormat(formatoSalida);
        formatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
        
 
        return formatUTC.format(date);
    }

    public static Cookie getNewCookieEncoded(String name, String value) {
        String valueEncoded = new BASE64Encoder().encode(value.getBytes()).replace(System.getProperty("line.separator"), "");
        return new Cookie(name, valueEncoded);
    }

    public static String getCookieValue(HttpServletRequest request, String key) {
        Cookie[] cookies = request.getCookies();

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(key)) {
                return cookie.getValue();
            }
        }
        return null;
    }

    public static String getCookieValueDecoded(HttpServletRequest request, String key) throws IOException {
        String valueEncoded = WebUtil.getCookieValue(request, key);
        if (valueEncoded == null) {
            return null;
        }

        // parche, por algun motivo se come los signos = y falla la decodificacion,
        // asi que si es impar le ponemos un = al final
        while (valueEncoded.length() % 4 != 0) {
            valueEncoded = valueEncoded + "=";
        }

        return new String(new BASE64Decoder().decodeBuffer(valueEncoded));
    }

    public static void closeStatements(ResultSet rs, PreparedStatement ps) {
        closeResultSet(rs);
        closePreparedStatement(ps);
    }

    public static void closePreparedStatement(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public static void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
