package com.movixla.broadcastmanager.aplicacion.manager;

import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcastmanager.aplicacion.BroadcastInfo;
import com.movixla.broadcastmanager.aplicacion.managerwrapper.ManagerWrapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Andres
 * Date: 13-jul-2009
 * Time: 12:08:30
 */
public class BroadcastInfoManager extends ManagerWrapper {

	/*
	 * Obtiene la informacion de un broadcast.
	 */
    public static BroadcastInfo getByBroadcast(Broadcast broadcast) throws SQLException {
        Connection connection = getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select  \n" +
                    "( select count(*) from acciones where broadcast_id=? and estado=1 ) as acciones, \n" +
                    "( select count(*) from notificaciones where broadcast_id=? and estado=1 ) as notificaciones, \n" +
                    "( select count(*) from restricciones where broadcast_id=? and estado=1 ) as restricciones");
            preparedStatement.setInt(1, broadcast.getId());
            preparedStatement.setInt(2, broadcast.getId());
            preparedStatement.setInt(3, broadcast.getId());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            BroadcastInfo info = null;
            if (resultSet.next()) {
                info = new BroadcastInfo(resultSet.getInt("acciones"), resultSet.getInt("notificaciones"), resultSet.getInt("restricciones"));
            }

            preparedStatement.close();
            resultSet.close();

            return info;
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

}
