package com.movixla.broadcastmanager.aplicacion.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.movix.shared.Operador;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;
import com.movixla.broadcastmanager.aplicacion.domobj.Perfil;


public class PerfilManager {

    public static Perfil getPerfilById(Connection connection, int id) throws SQLException {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM perfiles WHERE id=? limit 1");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            Perfil perfil = null;

            if (resultSet.next()) {
                perfil = rowMapper(resultSet);
            }

            preparedStatement.close();
            resultSet.close();

            return perfil;
        } finally {
        }
    }
    
    private static Perfil rowMapper(ResultSet rs) throws SQLException {

        String sope = rs.getString("operadores");
        List<Operador> lista = new ArrayList<Operador>();
        
        if (sope.equals("all")){
            Operador[] arr = Operador.values();
            for (int i=0;i<arr.length;i++){
                lista.add(arr[i]);
            }
        }else{
            String[] arr = sope.split(",");
            for (int i=0;i<arr.length;i++){
                lista.add(Operador.getOperadorPorIdBD(Integer.parseInt(arr[i])));
            }
            
        }
        Operador operadorDefecto = lista.get(0);
        int iope = rs.getInt("operador_vdefecto");
        if (iope > 0){
            operadorDefecto = Operador.getOperadorPorIdBD(iope);
        } 
        
        String stipos = rs.getString("mensaje_tipos");
        List<Mensaje.Tipo> lista2 = new ArrayList<Mensaje.Tipo>();
        
        if (stipos.equals("all")){
            Mensaje.Tipo[] arr = Mensaje.Tipo.values();
            for (int i=0;i<arr.length;i++){
                lista2.add(arr[i]);
            }
        }else{
            String[] arr = stipos.split(",");
            for (int i=0;i<arr.length;i++){
                lista2.add(Mensaje.Tipo.valueOf(arr[i]));
            }
            
        }
        Mensaje.Tipo tipoDefecto = lista2.get(0);
        stipos = rs.getString("mensaje_tipo_vdefecto");
        if (stipos != null && !stipos.trim().equals("")){
            tipoDefecto = Mensaje.Tipo.valueOf(stipos);
        } 
        
        return new Perfil(
                rs.getInt("id"),
                rs.getString("nombre"),
                rs.getString("nombreLogo"),
                rs.getString("imagenLogo"),
                lista,
                operadorDefecto,
                rs.getString("tasa_editable"),
                rs.getInt("tasa_vdefecto"),
                lista2,
                tipoDefecto,
                rs.getString("mensaje_la_editable"),
                rs.getString("mensaje_la_vdefecto"),
                rs.getString("mensaje_servicio_visible"),
                rs.getString("mensaje_servicio_editable"),
                rs.getString("mensaje_servicio_vdefecto"),
                rs.getString("mensaje_texto_editable"),
                rs.getString("mensaje_texto_vdefecto"),
                rs.getString("mensaje_smsfinal_editable"),
                rs.getString("mensaje_smsfinal_vdefecto"),
                rs.getString("mensaje_url_editable"),
                rs.getString("mensaje_url_vdefecto"),
                rs.getString("mensaje_sregala_editable"),
                rs.getString("mensaje_sregala_vdefecto"),
                rs.getString("listanegra_visible"),
                rs.getString("notificaciones_visible"),
                rs.getBoolean("validar_lnu"));
    }

}
