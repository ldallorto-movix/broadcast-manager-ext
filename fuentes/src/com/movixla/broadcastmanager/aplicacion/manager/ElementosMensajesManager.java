package com.movixla.broadcastmanager.aplicacion.manager;


import com.movixla.broadcast.shared.ElementoMensaje;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: rparedes
 * Date: Mar 4, 2011
 * Time: 11:04:03 AM
 */
public class ElementosMensajesManager {

    public static int addElementoMensaje(Connection connection, Mensaje mensaje, ElementoMensaje.Tipo tipo, String texto, String url, int duracion, ElementoMensaje.Estado estado) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            String sql = "insert into elementos_mensajes (id, mensaje_id, tipo, texto, url, duracion, estado) values (?,?,?,?,?,?,?)";
            String sqlId = "select nextval('elementos_mensajes_id_seq') as id";
            int elementoMensajeId = -1;
            PreparedStatement psId = connection.prepareStatement(sqlId);
            ResultSet rsId = psId.executeQuery();
            if (rsId.next()) {
                elementoMensajeId = rsId.getInt("id");
            }
            rsId.close();
            psId.close();
            if (0 < elementoMensajeId) {
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, elementoMensajeId);
                preparedStatement.setInt(2, mensaje.getId());
                preparedStatement.setString(3, tipo.name());
                preparedStatement.setString(4, texto);
                preparedStatement.setString(5, url);
                preparedStatement.setInt(6, duracion);
                preparedStatement.setInt(7, estado.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            return elementoMensajeId;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }


    public static ElementoMensaje getElementoMensajeById(Connection connection, int id) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from elementos_mensajes where id= ?");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            ElementoMensaje elementoMensaje = null;
            if (resultSet.next()) {
                elementoMensaje = rowMapper(resultSet);
            }
            preparedStatement.close();
            resultSet.close();
            return elementoMensaje;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static List<ElementoMensaje> getElementosMensajesByMensaje(Connection connection, Mensaje mensaje) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM elementos_mensajes WHERE mensaje_id = ? ORDER BY id asc ");
            preparedStatement.setInt(1, mensaje.getId());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            List<ElementoMensaje> elementos = new ArrayList<ElementoMensaje>();
            while (resultSet.next()) {
                elementos.add(rowMapper(resultSet));
            }
            preparedStatement.close();
            resultSet.close();
            return elementos;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static int updateURL(Connection connection, ElementoMensaje elementoMensaje, String pathImagen) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            String sql = "update elementos_mensajes set url=? where id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, pathImagen);
            preparedStatement.setInt(2, elementoMensaje.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return elementoMensaje.getId();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static int updateElementoMensaje(Connection connection, ElementoMensaje elementoMensaje, String texto, int duracion, ElementoMensaje.Estado estado) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            String sql = "update elementos_mensajes set texto = ?, duracion = ?, estado = ? where id = ? ";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, texto);
            preparedStatement.setInt(2, duracion);
            preparedStatement.setInt(3, estado.getId());
            preparedStatement.setInt(4, elementoMensaje.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return elementoMensaje.getId();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static void deleteElementoMensaje(Connection connection, ElementoMensaje elementoMensaje) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            String sql = "update elementos_mensajes set estado = ? WHERE id= ? ";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, ElementoMensaje.Estado.ELIMINADO.getId());
            preparedStatement.setInt(2, elementoMensaje.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    private static ElementoMensaje rowMapper(ResultSet rs) throws SQLException {
        ElementoMensaje elementoMensaje = new ElementoMensaje(
                rs.getInt("id"),
                rs.getInt("mensaje_id"),
                ElementoMensaje.Tipo.valueOf(rs.getString("tipo")),
                rs.getString("texto"),
                rs.getString("url"),
                rs.getInt("duracion"),
                ElementoMensaje.Estado.getById(rs.getInt("estado"))
        );
        return elementoMensaje;
    }

}
