package com.movixla.broadcastmanager.aplicacion.manager;

import com.movix.shared.Operador;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andres
 *         UTC Compliant
 */
public class UsuariosManager {

    public static void insertarNuevoOperador(Connection conn, int operador_id) throws SQLException {

        String sql = "insert into usuarios_operadores(usuario_id,operador_id) SELECT id," + operador_id + " from usuarios";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);

        preparedStatement.executeUpdate();

        preparedStatement.close();
    }

    public static int getMaximoFromUsuario(Connection conn) throws SQLException {

        int res = 1000;
        PreparedStatement preparedStatement = conn
                .prepareStatement("SELECT max(operador_id) as maxid from usuarios_operadores");

        preparedStatement.execute();
        ResultSet rs = preparedStatement.getResultSet();

        if (rs.next()) {
            res = rs.getInt("maxid");
        }

        preparedStatement.close();
        rs.close();

        return res;

    }

    public static Usuario getusuarioByUsernamePassword(Connection connection, String username, String password)
            throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT * FROM usuarios  WHERE username=? and password=md5(?)");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();

            Usuario usuario = null;

            if (rs.next()) {
                usuario = rowMapper(rs, getOperadoresDeUsuarioId(connection, rs.getInt("id")));
            }

            preparedStatement.close();
            rs.close();

            return usuario;
        }
        finally {
            // Tiempos.logTiempo(l);
        }
    }

    private static List<Operador> getOperadoresDeUsuarioId(Connection connection, int uid) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT operador_id FROM usuarios_operadores WHERE usuario_id=? ");
            preparedStatement.setInt(1, uid);
            preparedStatement.execute();

            List<Operador> operadores = new ArrayList<Operador>();

            ResultSet rs = preparedStatement.getResultSet();

            while (rs.next()) {
                int oid = rs.getInt("operador_id");

                Operador operador;
                if (oid == -1) {
                    operador = Operador.DUMMY;
                }
                else {
                    operador = Operador.getOperadorPorIdBD(oid);
                }

                if (operador != null) {
                    if (!operadores.contains(operador)) {
                        operadores.add(operador);
                    }
                }
            }

            preparedStatement.close();
            rs.close();

            return operadores;
        }
        finally {
            // Tiempos.logTiempo(l);
        }
    }

    private static Usuario rowMapper(ResultSet rs, List<Operador> operadores) throws SQLException {
        return new Usuario(rs.getInt("id"), 
                           rs.getString("username"), 
                           rs.getString("nombre"), 
                           Usuario.Tipo.getById(rs.getInt("tipo")), 
                           rs.getInt("perfil_id"));
    }

}
