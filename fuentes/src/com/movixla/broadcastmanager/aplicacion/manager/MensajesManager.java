package com.movixla.broadcastmanager.aplicacion.manager;

import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andres
 *         UTC Compliant
 */
public class MensajesManager {
    /*
        id SERIAL NOT NULL,
        broadcast_id INTEGER NOT NULL,
        tipo TEXT,
        servicio TEXT,
        precio TEXT,
        la TEXT,
        texto TEXT,
        url TEXT,
        mms TEXT,
        orden INTEGER,
    */
    public static int addMensaje(Connection connection, Broadcast broadcast, Mensaje.Tipo tipo, String servicio, String precio, String la, String texto, String url, int orden, Mensaje.Estado estado,String servicio_regala, String sms_final) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            String sql = "INSERT INTO mensajes (broadcast_id, tipo, servicio, precio, la, texto, url, orden, estado,servicio_regala, sms_final) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, broadcast.getId());
            preparedStatement.setString(2, tipo.name());
            preparedStatement.setString(3, servicio);
            preparedStatement.setString(4, precio);
            preparedStatement.setString(5, la);
            preparedStatement.setString(6, texto);
            preparedStatement.setString(7, url);
            preparedStatement.setInt(8, orden);
            preparedStatement.setInt(9, estado.getId());
            preparedStatement.setString(10, servicio_regala);
            preparedStatement.setString(11,sms_final);

            preparedStatement.executeUpdate();

            preparedStatement.close();

            //vemos con que id quedo la insercion.
            String sqlIdInserted = "SELECT currval('mensajes_id_seq')";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlIdInserted);

            int idInserted = -1;

            if (resultSet.next()) {
                idInserted = resultSet.getInt(1);
            }

            resultSet.close();

            return idInserted;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static Mensaje getMensajeById(Connection connection, int id) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM mensajes WHERE id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            Mensaje mensaje = null;

            if (resultSet.next()) {
                mensaje = rowMapper(resultSet);
            }

            preparedStatement.close();
            resultSet.close();

            return mensaje;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static List<Mensaje> getMensajeByBroadcast0(Connection connection, Broadcast broadcast) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM mensajes WHERE broadcast_id=? ORDER BY orden asc");
            preparedStatement.setInt(1, broadcast.getId());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            List<Mensaje> mensajes = new ArrayList<Mensaje>();

            while (resultSet.next()) {
                mensajes.add(rowMapper(resultSet));
            }

            preparedStatement.close();
            resultSet.close();

            return mensajes;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    public static Mensaje getMensajeByBroadcast(Connection connection, Broadcast broadcast) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM mensajes WHERE broadcast_id=? and estado = 1 limit 1");
            preparedStatement.setInt(1, broadcast.getId());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            Mensaje mensaje = null;

            while (resultSet.next()) {
                mensaje = rowMapper(resultSet);
            }

            preparedStatement.close();
            resultSet.close();

            return mensaje;
        } finally {
        }
    }

    public static List<Mensaje> getMensajeByBroadcast(Connection connection, Broadcast broadcast, Mensaje.Estado estado) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM mensajes WHERE broadcast_id=? AND estado=? ORDER BY orden asc");
            preparedStatement.setInt(1, broadcast.getId());
            preparedStatement.setInt(2, estado.getId());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            List<Mensaje> mensajes = new ArrayList<Mensaje>();

            while (resultSet.next()) {
                mensajes.add(rowMapper(resultSet));
            }

            preparedStatement.close();
            resultSet.close();

            return mensajes;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static int updateMensaje(Connection connection, Mensaje mensaje, Mensaje.Tipo tipo, String servicio, String precio, String la, String texto, String url, int orden, Mensaje.Estado estado,String servicio_regala, String sms_final) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            String sql = "UPDATE mensajes set tipo=?, servicio=?, precio=?, la=?, texto=?, url=?, orden=?, estado=?, servicio_regala=?, sms_final = ? WHERE id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, tipo.name());
            preparedStatement.setString(2, servicio);
            preparedStatement.setString(3, precio);
            preparedStatement.setString(4, la);
            preparedStatement.setString(5, texto);
            preparedStatement.setString(6, url);
            preparedStatement.setInt(7, orden);
            preparedStatement.setInt(8, estado.getId());
            preparedStatement.setString(9, servicio_regala);
            preparedStatement.setString(10, sms_final);
            
            preparedStatement.setInt(11, mensaje.getId());
            

            preparedStatement.executeUpdate();
            preparedStatement.close();

            return mensaje.getId();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static void deleteMensaje(Connection connection, Mensaje mensaje) throws SQLException {
        long l = System.currentTimeMillis();
        try {
            String sql = "UPDATE mensajes set estado=? WHERE id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, Mensaje.Estado.ELIMINADO.getId());
            preparedStatement.setInt(2, mensaje.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    private static Mensaje rowMapper(ResultSet rs) throws SQLException {
        Mensaje mensaje = new Mensaje(
                rs.getInt("id"),
                rs.getInt("broadcast_id"),
                Mensaje.Tipo.valueOf(rs.getString("tipo")),
                rs.getString("servicio"),
                rs.getString("precio"),
                rs.getString("la"),
                rs.getString("texto"),
                rs.getString("url"),
                rs.getInt("orden"),
                Mensaje.Estado.getById(rs.getInt("estado")),
                rs.getString("servicio_regala"),
                rs.getString("sms_final")
        );

        return mensaje;
    }


}
