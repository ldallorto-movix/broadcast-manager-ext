package com.movixla.broadcastmanager.aplicacion.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.movix.shared.Operador;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.Conexion;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;

/**
 * @author Andres
 *         UTC Compliant Maneja fechas de Java dentro de la BD => maquina y BD deben estar en misma zona horaria
 */
public class BroadcastsManager {
	
	/**
     * Crea un broadcast en la base de datos.
    */
    public static int addBroadcast(Connection connection, Usuario usuario, Broadcast parent, Operador operador, String nombre,
                                   String descripcion, Date fechaInicio, Date fechaFin, int repetirCada, String baseControl,
                                   int baseControlTasa, int tasa, Broadcast.Estado estado, Broadcast.BaseTipo baseTipo,
                                   String basePathArchivo, String baseSQL, Conexion baseDbConf, Broadcast.ListaNegraTipo listaNegraTipo, String listaNegraPathArchivo, String listaNegraSQL, Conexion listaNegraDbConf, int separacion_mensajes,
                                   String dwTipoProducto,int dwTipoProductoId,int dwMedioId,String service_code, boolean validarLnu) throws SQLException {

        // long l = System.currentTimeMillis();
        try {
            String sql = "INSERT INTO broadcasts (parent_id, usuario_id, operador, nombre, descripcion, " +
                    "fecha_creacion, fecha_inicio, fecha_fin, repetir_cada, base_control, base_control_tasa, estado, tasa_envio, " +
                    "base_tipo, base_path_archivo, base_sql, base_db_conf, lista_negra_tipo, lista_negra_path_archivo, lista_negra_sql, lista_negra_db_conf, aprobado, fecha_modificacion, separacion_mensajes,dwTipoProducto,dwTipoProductoId,dwMedioId,service_code,validar_lnu) VALUES " +
                    "(?, ?, ?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?,?,?,?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, parent != null ? parent.getId() : -1);
            preparedStatement.setInt(2, usuario != null ? usuario.getId() : -1);
            preparedStatement.setInt(3, operador.getIdBD());
            preparedStatement.setString(4, nombre);
            preparedStatement.setString(5, descripcion);
            preparedStatement.setTimestamp(6, new Timestamp(fechaInicio.getTime()));
            preparedStatement.setTimestamp(7, new Timestamp(fechaFin.getTime()));
            preparedStatement.setInt(8, repetirCada);
            preparedStatement.setString(9, baseControl);
            preparedStatement.setInt(10, baseControlTasa);
            preparedStatement.setInt(11, estado.getId());
            preparedStatement.setInt(12, tasa);
            preparedStatement.setString(13, baseTipo.name());
            preparedStatement.setString(14, basePathArchivo);
            preparedStatement.setString(15, baseSQL);
            preparedStatement.setString(16, baseDbConf != null ? baseDbConf.getId() : null);
            preparedStatement.setString(17, listaNegraTipo.name());
            preparedStatement.setString(18, listaNegraPathArchivo);
            preparedStatement.setString(19, listaNegraSQL);
            preparedStatement.setString(20, listaNegraDbConf != null ? listaNegraDbConf.getId() : null);
            preparedStatement.setInt(21, 0);
            preparedStatement.setInt(22, separacion_mensajes);

            preparedStatement.setString(23, dwTipoProducto);
            preparedStatement.setInt(24, dwTipoProductoId);
            preparedStatement.setInt(25, dwMedioId);
            preparedStatement.setString(26, service_code);

            preparedStatement.setBoolean(27, validarLnu);
            
            preparedStatement.executeUpdate();

            preparedStatement.close();

            //vemos con que id quedo la insercion.
            String sqlIdInserted = "SELECT currval('broadcasts_id_seq')";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlIdInserted);

            int idInserted = -1;

            if (resultSet.next()) {
                idInserted = resultSet.getInt(1);
            }

            resultSet.close();

            return idInserted;
        } finally {
            ////Tiempos.logTiempo(l);
        }
    }

    /**
     * Obtiene un objeto broadcast buscando por id.
    */
    public static Broadcast getBroadcastById(Connection connection, int id) throws SQLException {
        //// long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM broadcasts WHERE id=? limit 1");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            Broadcast broadcast = null;

            if (resultSet.next()) {
                broadcast = rowMapper(resultSet);
            }

            preparedStatement.close();
            resultSet.close();

            return broadcast;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    
    /**
     * Obtiene todos los broadcast de una DB.
    */
    public static List<Broadcast> getAllBroadcasts(Connection connection) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM broadcasts order by id desc");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            List<Broadcast> broadcasts = new ArrayList<Broadcast>();

            while (resultSet.next()) {
                broadcasts.add(rowMapper(resultSet));
            }

            preparedStatement.close();
            resultSet.close();

            return broadcasts;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static List<Broadcast> getAllBroadcastsNoEliminados(Connection connection, int usuarioId) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "(SELECT * FROM broadcasts where usuario_id = ? and estado !=? and estado !=? order by id desc)" +
                            "UNION ALL" +
                            "(SELECT * FROM broadcasts where usuario_id = ? and estado =? and age(fecha_creacion) < interval '4 month'  order by id desc)");
            preparedStatement.setInt(1, usuarioId);
            preparedStatement.setInt(2, Broadcast.Estado.ELIMINADO.getId());
            preparedStatement.setInt(3, Broadcast.Estado.TERMINADO.getId());
            preparedStatement.setInt(4, usuarioId);
            preparedStatement.setInt(5, Broadcast.Estado.TERMINADO.getId());
            
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            List<Broadcast> broadcasts = new ArrayList<Broadcast>();

            while (resultSet.next()) {
                broadcasts.add(rowMapper(resultSet));
            }

            preparedStatement.close();
            resultSet.close();
            return broadcasts;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static List<Broadcast> getAllBroadcastsParaProcesar(Connection connection) throws SQLException {
        // // long l = System.currentTimeMillis();
        try {
            //PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM broadcasts WHERE estado != ? AND estado != ? AND estado != ? AND fecha_inicio <= now() order by id desc");
        	PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM broadcasts WHERE estado not in (?,?,?) AND fecha_inicio <= now() order by id desc");
            preparedStatement.setInt(1, Broadcast.Estado.TERMINADO.getId());
            preparedStatement.setInt(2, Broadcast.Estado.ELIMINADO.getId());
            preparedStatement.setInt(3, Broadcast.Estado.CANCELADO.getId());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            List<Broadcast> broadcasts = new ArrayList<Broadcast>();

            while (resultSet.next()) {
                broadcasts.add(rowMapper(resultSet));
            }

            preparedStatement.close();
            resultSet.close();

            return broadcasts;
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    /**
     * Realiza la actualizacion de un broadcast en DB.
    */
    public static int updateBroadcast(Connection connection, Broadcast broadcast, Operador operador, String nombre,
                                      String descripcion, Date fechaInicio, Date fechaFin, int repetirCada, String baseControl,
                                      int baseControlTasa, int tasa, Broadcast.Estado estado, String estadoDetalle,
                                      Broadcast.BaseTipo baseTipo, String baseSQL, Conexion baseDb,
                                      Broadcast.ListaNegraTipo listaNegraTipo, String listaNegraSQL, Conexion listaNegraDb,
                                      boolean aprobado, int separacion_mensajes,String dwTipoProducto,int dwTipoProductoId,int dwMedioId,
                                      String service_code) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            String sql = "UPDATE broadcasts set operador=?, nombre=?, descripcion=?, " +
                    "fecha_inicio=?, fecha_fin=?, repetir_cada=?, base_control=?, base_control_tasa=?, estado=?, estado_detalle=?, tasa_envio=?, " +
                    "base_tipo=?, base_sql=?, base_db_conf=?, lista_negra_tipo=?, lista_negra_sql=?, lista_negra_db_conf=?, aprobado=?, fecha_modificacion=NOW(), separacion_mensajes=?,dwTipoProducto = ?,dwTipoProductoId= ?,dwMedioId = ?,service_code = ? WHERE id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, operador.getIdBD());
            preparedStatement.setString(2, nombre);
            preparedStatement.setString(3, descripcion);
            preparedStatement.setTimestamp(4, new Timestamp(fechaInicio.getTime()));
            preparedStatement.setTimestamp(5, new Timestamp(fechaFin.getTime()));
            preparedStatement.setInt(6, repetirCada);
            preparedStatement.setString(7, baseControl);
            preparedStatement.setInt(8, baseControlTasa);
            preparedStatement.setInt(9, estado.getId());
            preparedStatement.setString(10, estadoDetalle);
            preparedStatement.setInt(11, tasa);
            preparedStatement.setString(12, baseTipo.name());
            preparedStatement.setString(13, baseSQL);
            preparedStatement.setString(14, baseDb == null ? "-" : baseDb.getId());
            preparedStatement.setString(15, listaNegraTipo.name());
            preparedStatement.setString(16, listaNegraSQL);
            preparedStatement.setString(17, listaNegraDb == null ? "-" : listaNegraDb.getId());
            preparedStatement.setInt(18, aprobado ? 1 : 0);
            preparedStatement.setInt(19, separacion_mensajes);

            preparedStatement.setString(20, dwTipoProducto);
            preparedStatement.setInt(21, dwTipoProductoId);
            preparedStatement.setInt(22, dwMedioId);
            preparedStatement.setString(23, service_code);

            preparedStatement.setInt(24, broadcast.getId());

            preparedStatement.executeUpdate();
            preparedStatement.close();

            return broadcast.getId();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    /**
     * Realiza el update de un broadcast, campo basepatharchivo.
    */
    public static int updateBroadcastBaseArchivo(Connection connection, Broadcast broadcast, String basePathArchivo) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            String sql = "UPDATE broadcasts set base_path_archivo=?, fecha_modificacion=NOW() WHERE id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, basePathArchivo);
            preparedStatement.setInt(2, broadcast.getId());

            preparedStatement.executeUpdate();
            preparedStatement.close();

            return broadcast.getId();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    /**
     * Realiza el update de un broadcast, campo listanegrapatharchivo.
    */
    public static int updateBroadcastLNArchivo(Connection connection, Broadcast broadcast, String lnPathArchivo) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            String sql = "UPDATE broadcasts set lista_negra_path_archivo=?, fecha_modificacion=NOW() WHERE id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, lnPathArchivo);
            preparedStatement.setInt(2, broadcast.getId());

            preparedStatement.executeUpdate();
            preparedStatement.close();

            return broadcast.getId();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    public static int updateBroadcastListaNegraArchivo(Connection connection, Broadcast broadcast, String basePathArchivo) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            String sql = "UPDATE broadcasts set lista_negra_path_archivo=?, fecha_modificacion=NOW() WHERE id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, basePathArchivo);
            preparedStatement.setInt(2, broadcast.getId());

            preparedStatement.executeUpdate();
            preparedStatement.close();

            return broadcast.getId();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static int updateBroadcastEstado(Connection connection, Broadcast broadcast, Broadcast.Estado estado, String estadoDetalle) throws SQLException {
        return updateBroadcastEstado(connection, broadcast, estado, estadoDetalle, true);
    }
    
    /**
     * Realiza el update de un broadcast, campo estado.
    */
    public static int updateBroadcastEstado(Connection connection, Broadcast broadcast, Broadcast.Estado estado, String estadoDetalle, boolean modificado) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            PreparedStatement ps;
            if (estado == null) {
                //No cambiar el estado, solo el texto
                String sql = "UPDATE broadcasts set estado_detalle=?, fecha_modificacion=NOW() WHERE id=?";
                sql = !modificado ? "UPDATE broadcasts set estado_detalle=? WHERE id=?" : sql;
                ps = connection.prepareStatement(sql);
                ps.setString(1, estadoDetalle != null ? estadoDetalle : broadcast.getEstadoDetalle());
                ps.setInt(2, broadcast.getId());
            } else {
                String sql = "UPDATE broadcasts set estado=?, estado_detalle=?, fecha_modificacion=NOW() WHERE id=?";
                sql = !modificado ? "UPDATE broadcasts set estado=?, estado_detalle=? WHERE id=?" : sql;
                ps = connection.prepareStatement(sql);
                ps.setInt(1, estado.getId());
                ps.setString(2, estadoDetalle != null ? estadoDetalle : broadcast.getEstadoDetalle());
                ps.setInt(3, broadcast.getId());
            }

            ps.executeUpdate();
            ps.close();

            return broadcast.getId();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    public static void updateBroadcastEstadisticasEnvio(Connection connection, Broadcast broadcast) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            PreparedStatement ps;

            String sqlEnvios = "select \tcount(*) as totales,\n" +
                    "count( case when estado=1 then 1 else null end) as ok,\n" +
                    "count( case when estado=2 then 2 when estado=4 then 4 else null end) as error,\n" +
                    "count( case when estado=3 then 3 else null end) as listanegra,\n" +
                    "count( case when estado=5 then 5 else null end) as lnu\n" +
                    " from envios where broadcast_id=?";
            ps = connection.prepareStatement(sqlEnvios);
            ps.setInt(1, broadcast.getId());

            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            int enviosTotal = 0;
            int enviosOk = 0;
            int enviosError = 0;
            int enviosListaNegra = 0;
            int enviosLNU = 0;

            while (resultSet.next()) {
                enviosTotal = resultSet.getInt("totales");
                enviosOk = resultSet.getInt("ok");
                enviosError = resultSet.getInt("error");
                enviosListaNegra = resultSet.getInt("listanegra");
                enviosLNU = resultSet.getInt("lnu");
            }
            resultSet.close();
            ps.close();

            String sql = "UPDATE broadcasts set envios_total=?, envios_ok=?, envios_error=?, envios_listanegra=?,envios_lnu=?  WHERE id=?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, enviosTotal);
            ps.setInt(2, enviosOk);
            ps.setInt(3, enviosError);
            ps.setInt(4, enviosListaNegra);
            ps.setInt(5, enviosLNU);
            ps.setInt(6, broadcast.getId());

            ps.executeUpdate();
            ps.close();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    /**
     * Realiza el update de un broadcast, campo de estadistica de proceso del broadcast.
    */
    public static void updateBroadcastEstadisticasEnvio(Connection connection, Broadcast broadcast,long total,long ok,long error) throws SQLException {
       
        try {
            PreparedStatement ps;

            String sql = "UPDATE broadcasts set envios_total=?, envios_ok=?, envios_error=?, envios_listanegra=0  WHERE id=?";
            ps = connection.prepareStatement(sql);
            ps.setLong(1, total);
            ps.setLong(2, ok);
            ps.setLong(3, error);
            //ps.setLong(4, lnu);
            ps.setInt(4, broadcast.getId());

            ps.executeUpdate();
            ps.close();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    public static void updateBroadcastEstadisticasLNU(Connection connection, Broadcast broadcast,long lnu) throws SQLException {
        
        try {
            PreparedStatement ps;

            String sql = "UPDATE broadcasts set envios_lnu= envios_lnu+?  WHERE id=?";
            ps = connection.prepareStatement(sql);
            ps.setLong(1, lnu );
      
            ps.setInt(2, broadcast.getId());

            ps.executeUpdate();
            ps.close();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    public static void updateBroadcastEstadisticasLNPropia(Connection connection, Broadcast broadcast,long ln) throws SQLException {
        
        try {
            PreparedStatement ps;

            String sql = "UPDATE broadcasts set envios_listanegra=?  WHERE id=?";
            ps = connection.prepareStatement(sql);
            ps.setLong(1, ln);
      
            ps.setInt(2, broadcast.getId());

            ps.executeUpdate();
            ps.close();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }
    
    public static void updateBroadcastEstadisticasEnvio_sinTotal(Connection connection, Broadcast broadcast,long ok,long error) throws SQLException {
        
        try {
            PreparedStatement ps;

            String sql = "UPDATE broadcasts set envios_ok=?, envios_error=? WHERE id=?";
            ps = connection.prepareStatement(sql);
            
            ps.setLong(1, ok);
            ps.setLong(2, error);
            //ps.setLong(3, lnu);
            ps.setInt(3, broadcast.getId());

            ps.executeUpdate();
            ps.close();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    public static Broadcast getBroadcastPadre(Connection connection, Broadcast broadcast) throws SQLException {
        if (broadcast.getParentId() > 0) {
            return getBroadcastById(connection, broadcast.getParentId());
        }

        return null;
    }

    /**
     * Realiza el update de un broadcast, campo fecha termino.
    */
    public static void updateBroadcastFechaTermino(Connection connection, Broadcast broadcast) throws SQLException {
        // long l = System.currentTimeMillis();
        try {
            String sql = "UPDATE broadcasts set fecha_termino=NOW() WHERE id=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, broadcast.getId());

            ps.executeUpdate();
            ps.close();
        } finally {
            //Tiempos.logTiempo(l);
        }
    }

    private static Broadcast rowMapper(ResultSet rs) throws SQLException {
        Operador operador = Operador.getOperadorPorIdBD(rs.getInt("operador"));
        if (operador == null) operador = Operador.DUMMY;
        return new Broadcast(
                rs.getInt("id"),
                rs.getInt("parent_id"),
                null, //usuario
                operador,
                rs.getString("nombre"),
                rs.getString("descripcion"),
                rs.getTimestamp("fecha_creacion"),
                rs.getTimestamp("fecha_inicio"),
                rs.getTimestamp("fecha_fin"),
                rs.getInt("repetir_cada"),
                rs.getString("base_control"),
                rs.getInt("base_control_tasa"),
                rs.getInt("tasa_envio"),
                Broadcast.Estado.getById(rs.getInt("estado")),
                rs.getString("estado_detalle"),
                Broadcast.BaseTipo.valueOf(rs.getString("base_tipo")),
                rs.getString("base_path_archivo"),
                rs.getString("base_sql"),
                null,
                Broadcast.ListaNegraTipo.valueOf(rs.getString("lista_negra_tipo")),
                rs.getString("lista_negra_path_archivo"),
                rs.getString("lista_negra_sql"),
                null,
                rs.getInt("aprobado") == 1,
                rs.getTimestamp("fecha_modificacion"),
                rs.getTimestamp("fecha_termino"),
                rs.getInt("envios_total"),
                rs.getInt("envios_ok"),
                rs.getInt("envios_error"),
                rs.getInt("envios_listanegra"),
                rs.getInt("separacion_mensajes"),
                rs.getInt("envios_lnu"),
                rs.getString("dwTipoProducto"),
                rs.getInt("dwTipoProductoId"),
                rs.getInt("dwMedioId"),
                rs.getString("service_code")
        );
    }

}
