package com.movixla.broadcastmanager.aplicacion;

import com.movix.shared.util.Configuracion;
import com.movixla.broadcast.client.ClienteBroadcast;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.Conexion;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by Andres
 * Date: 13-jul-2009
 * Time: 12:08:11
 */
public class ClienteBroadcastManager {
    ClienteBroadcast cb5;


    private static Logger logger = Logger.getLogger(ClienteBroadcastManager.class);

    private static ClienteBroadcastManager ourInstanceBCM5 = new ClienteBroadcastManager("bcm5");

    /*
     * Inicializa el cliente broadcast manager
     */
    private ClienteBroadcastManager(String bcm) {
        if ("bcm5".equals(bcm)) {
            logger.info("cb(BCM5) = new ClienteBroadcast(" + Configuracion.get(bcm + ".contenedorBroadcast.host") + ", " + Configuracion.get(bcm + ".contenedorBroadcast.id") + ")");
            cb5 = new ClienteBroadcast(Configuracion.get(bcm + ".contenedorBroadcast.host"), Configuracion.get(bcm + ".contenedorBroadcast.id"));
            logger.info("cb5 = " + cb5);
        } 

    }

    /*
     * Obtiene una instancia de cliente.
     */
    public static ClienteBroadcastManager getInstance(String bcm) {

        if ("bcm5".equals(bcm)) {
            logger.info("return ourInstanceBCM5 = " + ourInstanceBCM5);
            return ourInstanceBCM5;
        } else {
            logger.info("return null");
            return null;
        }

    }

    /*
     * Funcion de prueba
     */
    public void probar(Broadcast broadcast, List<String> moviles, String bcm) throws Exception {
        if ("bcm5".equals(bcm)) {
            logger.info("cb5.enviarPrueba(broadcast, moviles);" + cb5);
            cb5.enviarPrueba(broadcast, moviles);
        } 

    }
}
