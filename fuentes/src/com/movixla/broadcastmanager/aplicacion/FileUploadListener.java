package com.movixla.broadcastmanager.aplicacion;

import org.apache.commons.fileupload.ProgressListener;

import java.io.File;

/**
 * Created by Andres
 * Date: 02-jun-2009
 * Time: 10:47:05
 */
public class FileUploadListener implements ProgressListener {
    private volatile long bytesRead = 0L;
    private volatile long contentLength = 0L;
    private volatile long item = 0L;
    private volatile String badline = "";

	private Status status;
    private File file;

    /*
     * Constructor sin parametros
     */
    public FileUploadListener() {
        super();
        status = Status.UPLOADING;
        file = null;
    }

    public void update(long aBytesRead, long aContentLength, int anItem) {
        bytesRead = aBytesRead;
        contentLength = aContentLength;
        item = anItem;
    }

    /*
     * Obtiene los bytesread
     */
    public long getBytesRead() {
        return bytesRead;
    }

    public long getContentLength() {
        return contentLength;
    }

    public long getItem() {
        return item;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
    public String getBadline() {
		return badline;
	}

	public void setBadline(String badline) {
		this.badline = badline;
	}

    public enum Status {
        UPLOADING,
        FINISHED,
        CHECKING,
        ERROR;
    }
}
