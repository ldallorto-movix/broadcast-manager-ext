package com.movixla.broadcastmanager.aplicacion.domobj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.movixla.broadcast.shared.ElementoMensaje;

/**
 * @author Francisco
 *         UTC Compliant
 */
public class Mensaje implements Serializable {

    private int id;
    private int broadcastId;
    private Tipo tipo;
    private String servicio;
    private String precio;
    private String la;
    private String texto;
    private String url;
    private int orden;
    private Estado estado;
    private String servicioRegala;
    private List<ElementoMensaje> elementosMensajes = new ArrayList<ElementoMensaje>();
    private String smsFinal;

    public Mensaje(int id, int broadcastId, Tipo tipo, String servicio, String precio, String la, String texto, String url,
            int orden, Estado estado, String servicioRegala, String smsFinal) {
        this.id = id;
        this.broadcastId = broadcastId;
        this.tipo = tipo;
        this.servicio = servicio;
        this.precio = precio;
        this.la = la;
        this.texto = texto;
        this.url = url;
        this.orden = orden;
        this.estado = estado;
        this.servicioRegala = servicioRegala;
        this.smsFinal = smsFinal;
    }

    public String getSmsFinal() {
        return smsFinal;
    }

    public void setSmsFinal(String smsFinal) {
        this.smsFinal = smsFinal;
    }

    public String getServicioRegala() {
        return servicioRegala;
    }

    public void setServicioRegala(String servicioRegala) {
        this.servicioRegala = servicioRegala;
    }

    public int getId() {
        return id;
    }

    public int getBroadcastId() {
        return broadcastId;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public String getServicio() {
        return servicio;
    }

    public String getPrecio() {
        return precio;
    }

    public String getLa() {
        return la;
    }

    public String getTexto() {
        return texto;
    }

    public String getUrl() {
        return url;
    }

    public int getOrden() {
        return orden;
    }

    public Estado getEstado() {
        return estado;
    }

    public List<ElementoMensaje> getElementosMensajes() {
        return elementosMensajes;
    }

    public void setElementosMensajes(List<ElementoMensaje> elementosMensajes) {
        this.elementosMensajes = elementosMensajes;
    }

    public String toString() {
        return "Mensaje{" + "id=" + id + ", broadcastId=" + broadcastId + ", tipo=" + tipo + ", servicio='" + servicio + '\''
                + ", precio='" + precio + '\'' + ", la='" + la + '\'' + ", texto='" + texto + '\'' + ", url='" + url + '\''
                + ", orden=" + orden + ", estado=" + estado + ", sr=" + servicioRegala + '}';
    }

    public enum Estado implements Serializable {
        NUEVO(0), GUARDADO(1), ELIMINADO(2), ;

        private int id;

        Estado(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static Estado getById(int i) {
            Estado[] estados = values();
            for (Estado estado : estados) {
                if (estado.getId() == i)
                    return estado;
            }

            return null;
        }
    }

    public enum Tipo implements Serializable {
        SMS, WAPPUSH, MMS, REGALA, SATPUSH, SMS_SACA;

    }

}
