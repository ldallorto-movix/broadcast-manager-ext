package com.movixla.broadcastmanager.aplicacion.domobj;

import java.io.Serializable;


/**
 * @author Andres
 *         UTC Compliant
 */
public class Usuario implements Serializable {
    private int id;
    private String username;
    private String nombre;
    private Tipo tipo;
    private int perfilId;
    

    public Usuario(int id, String username, String nombre, Tipo tipo, int perfilId) {
        this.id = id;
        this.username = username;
        this.nombre = nombre;
        this.tipo = tipo;
        this.perfilId = perfilId;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getNombre() {
        return nombre;
    }

    public Tipo getTipo() {
        return tipo;
    }
    
    public int getPerfilId() {
        return perfilId;
    }

    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", nombre='" + nombre + '\'' +
                ", tipo=" + tipo +
                ", perfil=" + perfilId +
                '}';
    }

    public enum Tipo implements Serializable {
        NORMAL(0),
        ADMIN(1);

        private int id;

        Tipo(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static Tipo getById(int i) {
            Tipo[] tipos = values();
            for (Tipo tipo : tipos) {
                if (tipo.getId() == i) return tipo;
            }
            return null;
        }
    }
}
