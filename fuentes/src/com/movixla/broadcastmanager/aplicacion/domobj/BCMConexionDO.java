package com.movixla.broadcastmanager.aplicacion.domobj;

/**
 * Created by IntelliJ IDEA.
 * User: rparedes
 * Date: Dec 9, 2010
 * Time: 9:56:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class BCMConexionDO {
    String id;
    String nombre;

    /*
     * Contructor 
     */
    public BCMConexionDO(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    /*
     * Obtiene el id
     */
    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}