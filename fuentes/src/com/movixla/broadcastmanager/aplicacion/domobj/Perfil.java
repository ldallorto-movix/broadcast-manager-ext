package com.movixla.broadcastmanager.aplicacion.domobj;

import java.util.List;

import com.movix.shared.Operador;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje.Tipo;


public class Perfil {

    private int id;
    private String nombre;
    private String nombreLogo;
    private String imagenLogo;
    private List<Operador> operadores;
    private Operador operador_vdefecto;
    private String tasa_editable;
    private int tasa_vdefecto;
    private List<Mensaje.Tipo> mensaje_tipos;
    private Mensaje.Tipo mensaje_tipo_vdefecto;
    private String mensaje_la_editable;
    private String mensaje_la_vdefecto;
    private String mensaje_servicio_visible;
    private String mensaje_servicio_editable;
    private String mensaje_servicio_vdefecto;
    private String mensaje_texto_editable;
    private String mensaje_texto_vdefecto;
    private String mensaje_smsfinal_editable;
    private String mensaje_smsfinal_vdefecto;
    private String mensaje_url_editable;
    private String mensaje_url_vdefecto;
    private String mensaje_sregala_editable;
    private String mensaje_sregala_vdefecto;
    private String listanegra_visible;
    private String notificaciones_visible;
    private boolean validar_lnu;
    
    
    

    public Perfil(int id, String nombre, String nombreLogo, String imagenLogo, List<Operador> operadores,
            Operador operador_vdefecto, String tasa_editable, int tasa_vdefecto, List<Tipo> mensaje_tipos,
            Tipo mensaje_tipo_vdefecto, String mensaje_la_editable, String mensaje_la_vdefecto,
            String mensaje_servicio_visible, String mensaje_servicio_editable, String mensaje_servicio_vdefecto,
            String mensaje_texto_editable, String mensaje_texto_vdefecto, String mensaje_smsfinal_editable,
            String mensaje_smsfinal_vdefecto, String mensaje_url_editable, String mensaje_url_vdefecto,
            String mensaje_sregala_editable, String mensaje_sregala_vdefecto, String listanegra_visible,
            String notificaciones_visible, boolean validar_lnu) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.nombreLogo = nombreLogo;
        this.imagenLogo = imagenLogo;
        this.operadores = operadores;
        this.operador_vdefecto = operador_vdefecto;
        this.tasa_editable = tasa_editable;
        this.tasa_vdefecto = tasa_vdefecto;
        this.mensaje_tipos = mensaje_tipos;
        this.mensaje_tipo_vdefecto = mensaje_tipo_vdefecto;
        this.mensaje_la_editable = mensaje_la_editable;
        this.mensaje_la_vdefecto = mensaje_la_vdefecto;
        this.mensaje_servicio_visible = mensaje_servicio_visible;
        this.mensaje_servicio_editable = mensaje_servicio_editable;
        this.mensaje_servicio_vdefecto = mensaje_servicio_vdefecto;
        this.mensaje_texto_editable = mensaje_texto_editable;
        this.mensaje_texto_vdefecto = mensaje_texto_vdefecto;
        this.mensaje_smsfinal_editable = mensaje_smsfinal_editable;
        this.mensaje_smsfinal_vdefecto = mensaje_smsfinal_vdefecto;
        this.mensaje_url_editable = mensaje_url_editable;
        this.mensaje_url_vdefecto = mensaje_url_vdefecto;
        this.mensaje_sregala_editable = mensaje_sregala_editable;
        this.mensaje_sregala_vdefecto = mensaje_sregala_vdefecto;
        this.listanegra_visible = listanegra_visible;
        this.notificaciones_visible = notificaciones_visible;
        this.validar_lnu = validar_lnu;
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getNombreLogo() {
        return nombreLogo;
    }
    
    public void setNombreLogo(String nombreLogo) {
        this.nombreLogo = nombreLogo;
    }
    
    public String getImagenLogo() {
        return imagenLogo;
    }
    
    public void setImagenLogo(String imagenLogo) {
        this.imagenLogo = imagenLogo;
    }
    
    public List<Operador> getOperadores() {
        return operadores;
    }
    
    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }
    
    public String getTasa_editable() {
        return tasa_editable;
    }
    
    public void setTasa_editable(String tasa_editable) {
        this.tasa_editable = tasa_editable;
    }
     
    public int getTasa_vdefecto() {
        return tasa_vdefecto;
    }

    public void setTasa_vdefecto(int tasa_vdefecto) {
        this.tasa_vdefecto = tasa_vdefecto;
    }

    public List<Mensaje.Tipo> getMensaje_tipos() {
        return mensaje_tipos;
    }

    public void setMensaje_tipos(List<Mensaje.Tipo> mensaje_tipos) {
        this.mensaje_tipos = mensaje_tipos;
    }

    public String getMensaje_la_editable() {
        return mensaje_la_editable;
    }
    
    public void setMensaje_la_editable(String mensaje_la_editable) {
        this.mensaje_la_editable = mensaje_la_editable;
    }
    
    public String getMensaje_la_vdefecto() {
        return mensaje_la_vdefecto;
    }
    
    public void setMensaje_la_vdefecto(String mensaje_la_vdefecto) {
        this.mensaje_la_vdefecto = mensaje_la_vdefecto;
    }
    
    public String getMensaje_servicio_visible() {
        return mensaje_servicio_visible;
    }
    
    public void setMensaje_servicio_visible(String mensaje_servicio_visible) {
        this.mensaje_servicio_visible = mensaje_servicio_visible;
    }
    
    public String getMensaje_servicio_editable() {
        return mensaje_servicio_editable;
    }
    
    public void setMensaje_servicio_editable(String mensaje_servicio_editable) {
        this.mensaje_servicio_editable = mensaje_servicio_editable;
    }
    
    public String getMensaje_servicio_vdefecto() {
        return mensaje_servicio_vdefecto;
    }
    
    public void setMensaje_servicio_vdefecto(String mensaje_servicio_vdefecto) {
        this.mensaje_servicio_vdefecto = mensaje_servicio_vdefecto;
    }
    
    public String getMensaje_texto_editable() {
        return mensaje_texto_editable;
    }
    
    public void setMensaje_texto_editable(String mensaje_texto_editable) {
        this.mensaje_texto_editable = mensaje_texto_editable;
    }
    
    public String getMensaje_texto_vdefecto() {
        return mensaje_texto_vdefecto;
    }
    
    public void setMensaje_texto_vdefecto(String mensaje_texto_vdefecto) {
        this.mensaje_texto_vdefecto = mensaje_texto_vdefecto;
    }
    
    public String getMensaje_smsfinal_editable() {
        return mensaje_smsfinal_editable;
    }
    
    public void setMensaje_smsfinal_editable(String mensaje_smsfinal_editable) {
        this.mensaje_smsfinal_editable = mensaje_smsfinal_editable;
    }
    
    public String getMensaje_smsfinal_vdefecto() {
        return mensaje_smsfinal_vdefecto;
    }
    
    public void setMensaje_smsfinal_vdefecto(String mensaje_smsfinal_vdefecto) {
        this.mensaje_smsfinal_vdefecto = mensaje_smsfinal_vdefecto;
    }
    
    public String getMensaje_url_editable() {
        return mensaje_url_editable;
    }
    
    public void setMensaje_url_editable(String mensaje_url_editable) {
        this.mensaje_url_editable = mensaje_url_editable;
    }
    
    public String getMensaje_url_vdefecto() {
        return mensaje_url_vdefecto;
    }
    
    public void setMensaje_url_vdefecto(String mensaje_url_vdefecto) {
        this.mensaje_url_vdefecto = mensaje_url_vdefecto;
    }
    
    public String getListanegra_visible() {
        return listanegra_visible;
    }
    
    public void setListanegra_visible(String listanegra_visible) {
        this.listanegra_visible = listanegra_visible;
    }
    
    public String getNotificaciones_visible() {
        return notificaciones_visible;
    }
    
    public void setNotificaciones_visible(String notificaciones_visible) {
        this.notificaciones_visible = notificaciones_visible;
    }
    
    public boolean isValidar_lnu() {
        return validar_lnu;
    }

    public void setValidar_lnu(boolean validar_lnu) {
        this.validar_lnu = validar_lnu;
    }

    public Operador getOperador_vdefecto() {
        return operador_vdefecto;
    }

    
    public void setOperador_vdefecto(Operador operador_vdefecto) {
        this.operador_vdefecto = operador_vdefecto;
    }

    
    public Mensaje.Tipo getMensaje_tipo_vdefecto() {
        return mensaje_tipo_vdefecto;
    }

    
    public void setMensaje_tipo_vdefecto(Mensaje.Tipo mensaje_tipo_vdefecto) {
        this.mensaje_tipo_vdefecto = mensaje_tipo_vdefecto;
    }

    public String getMensaje_sregala_editable() {
        return mensaje_sregala_editable;
    }

    
    public void setMensaje_sregala_editable(String mensaje_sregala_editable) {
        this.mensaje_sregala_editable = mensaje_sregala_editable;
    }

    
    public String getMensaje_sregala_vdefecto() {
        return mensaje_sregala_vdefecto;
    }

    
    public void setMensaje_sregala_vdefecto(String mensaje_sregala_vdefecto) {
        this.mensaje_sregala_vdefecto = mensaje_sregala_vdefecto;
    }
    

    @Override
    public String toString() {
        return "Perfil [id=" + id + ", nombre=" + nombre + ", nombreLogo=" + nombreLogo + ", imagenLogo=" + imagenLogo
                + ", operadores=" + operadores + ", tasa_editable=" + tasa_editable + ", tasa_vdefecto=" + tasa_vdefecto
                + ", mensaje_tipos=" + mensaje_tipos + ", mensaje_tipo_vdefecto=" + mensaje_tipo_vdefecto
                + ", mensaje_la_editable=" + mensaje_la_editable + ", mensaje_la_vdefecto=" + mensaje_la_vdefecto
                + ", mensaje_servicio_visible=" + mensaje_servicio_visible + ", mensaje_servicio_editable="
                + mensaje_servicio_editable + ", mensaje_servicio_vdefecto=" + mensaje_servicio_vdefecto
                + ", mensaje_texto_editable=" + mensaje_texto_editable + ", mensaje_texto_vdefecto=" + mensaje_texto_vdefecto
                + ", mensaje_smsfinal_editable=" + mensaje_smsfinal_editable + ", mensaje_smsfinal_vdefecto="
                + mensaje_smsfinal_vdefecto + ", mensaje_url_editable=" + mensaje_url_editable + ", mensaje_url_vdefecto="
                + mensaje_url_vdefecto + ", mensaje_sregala_editable=" + mensaje_sregala_editable
                + ", mensaje_sregala_vdefecto=" + mensaje_sregala_vdefecto + ", listanegra_visible=" + listanegra_visible
                + ", notificaciones_visible=" + notificaciones_visible + ", validarlnu=" + validar_lnu + "]";
    }

}
