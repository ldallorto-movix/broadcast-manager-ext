package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.movixla.broadcastmanager.aplicacion.domobj.Perfil;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.manager.PerfilManager;


public class PerfilManagerWrapper extends ManagerWrapper{

    private static Logger logger = Logger.getLogger(PerfilManagerWrapper.class);
    
    public static Perfil getPerfilForUser(Usuario usuario, String bcm) throws SQLException{
        
        logger.info("bcm=" + bcm + ".bd");
        Connection connection = getConnection(bcm + ".bd");
        try {
            return PerfilManager.getPerfilById(connection,usuario.getPerfilId());
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
        
    }

}
