package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import com.movixla.broadcast.shared.ElementoMensaje;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;
import com.movixla.broadcastmanager.aplicacion.manager.ElementosMensajesManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: rparedes
 * Date: Mar 4, 2011
 * Time: 11:16:57 AM
 * To change this template use File | Settings | File Templates.
 */
public class ElementosMensajesManagerWrapper extends ManagerWrapper {

	/*
	 * A�ade un elemento al mensaje del broadcast.
	 */
    public static int addElementoMensaje(Mensaje mensaje, ElementoMensaje.Tipo tipo, String texto, String url, int duracion, ElementoMensaje.Estado estado, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return ElementosMensajesManager.addElementoMensaje(connection, mensaje, tipo, texto, url, duracion, estado);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    /*
     * Obtiene un elemento mensaje por id.
     */
    public static ElementoMensaje getElementoMensajeById(int id, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return ElementosMensajesManager.getElementoMensajeById(connection, id);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    /*
     * Obtiene todos los elementos mensajes de un mensaje.
     */
    public static List<ElementoMensaje> getElementosMensajesByMensaje(Mensaje mensaje, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return ElementosMensajesManager.getElementosMensajesByMensaje(connection, mensaje);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    public static int updateURL(ElementoMensaje elementoMensaje, String pathImagen, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return ElementosMensajesManager.updateURL(connection, elementoMensaje, pathImagen);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    public static int updateElementoMensaje(ElementoMensaje elementoMensaje, String texto, int duracion, ElementoMensaje.Estado estado, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return ElementosMensajesManager.updateElementoMensaje(connection, elementoMensaje, texto, duracion, estado);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    public static void deleteElementoMensaje(ElementoMensaje elementoMensaje, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            ElementosMensajesManager.deleteElementoMensaje(connection, elementoMensaje);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }
}
