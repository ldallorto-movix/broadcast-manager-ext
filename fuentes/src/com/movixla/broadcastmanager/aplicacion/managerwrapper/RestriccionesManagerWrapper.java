package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.Restriccion;
import com.movixla.broadcast.shared.manager.RestriccionesManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Andres
 * Date: 25-may-2009
 * Time: 18:36:36
 */
public class RestriccionesManagerWrapper extends ManagerWrapper {
    
	/*
	 * A�ade una restriccion a la bd.
	 */
	public static int addRestricion(Broadcast broadcast, Date fechaInicio, Date fechaFin, double tasaEnvio, Restriccion.Estado estado, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return RestriccionesManager.addRestricion(connection, broadcast, fechaInicio, fechaFin, tasaEnvio, estado);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

	/*
	 * Obtiene una restriccion por id
	 */
    public static Restriccion getRestriccionById(int id) throws SQLException {
        Connection connection = getConnection();
        try {
            return RestriccionesManager.getRestriccionById(connection, id);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

    /*
     * Obtiene toda las restricciones configuradas para un broadcast.
     */
    public static List<Restriccion> getRestriccionesByBroadcast(Broadcast broadcast, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return RestriccionesManager.getRestriccionesByBroadcast(connection, broadcast);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

    /*
     * Actualiza una restriccion en bd.
     */
    public static int updateRestriccion(Restriccion restriccion, Date fechaInicio, Date fechaFin, double tasaEnvio, Restriccion.Estado estado, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return RestriccionesManager.updateRestriccion(connection, restriccion, fechaInicio, fechaFin, tasaEnvio, estado);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

    public static void deleteRestriccion(Restriccion restriccion, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            RestriccionesManager.deleteRestriccion(connection, restriccion);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }


}
