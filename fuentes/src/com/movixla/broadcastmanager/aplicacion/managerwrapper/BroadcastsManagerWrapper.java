package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import com.movix.shared.Operador;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.Conexion;
import com.movixla.broadcastmanager.aplicacion.bo.BCMConexionBO;
import com.movixla.broadcastmanager.aplicacion.domobj.BCMConexionDO;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.manager.BroadcastsManager;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Andres
 * Date: 25-may-2009
 * Time: 18:11:58
 */
public class BroadcastsManagerWrapper extends ManagerWrapper {

    private static Logger logger = Logger.getLogger(BroadcastsManagerWrapper.class);

    /*
     * A�ade un broadcast a la BD.
     */
    public static int addBroadcast(Usuario usuario, Broadcast parent, Operador operador, String nombre,
                                   String descripcion, Date fechaInicio, Date fechaFin, int repetirCada, String baseControl,
                                   int baseControlTasa, int tasa, Broadcast.Estado estado, Broadcast.BaseTipo baseTipo,
                                   String basePathArchivo, String baseSQL, Conexion baseDbConf, Broadcast.ListaNegraTipo listaNegraTipo,
                                   String listaNegraPathArchivo, String listaNegraSQL, Conexion listaNegraDbConf,
                                   int separacion_mensajes, String bcm,String dwTipoProducto,int dwTipoProductoId,int dwMedioId,
                                   String service_code, boolean validarLnu) throws SQLException {

        logger.info("bcm=" + bcm + ".bd");
        Connection connection = getConnection(bcm + ".bd");
        try {
            return BroadcastsManager.addBroadcast(connection, usuario, parent, operador, nombre, descripcion, fechaInicio, fechaFin, repetirCada, baseControl, baseControlTasa, tasa, estado, baseTipo, basePathArchivo, baseSQL, baseDbConf,
                    listaNegraTipo, listaNegraPathArchivo, listaNegraSQL, listaNegraDbConf, separacion_mensajes,dwTipoProducto,dwTipoProductoId,dwMedioId,service_code, validarLnu);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    /*
     * Obtiene el broadcast de la base de datos por id.
     */
    public static Broadcast getBroadcastById(int id, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return BroadcastsManager.getBroadcastById(connection, id);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    /*
     * Obtiene toda la lista de broadcast de la bd.
     */
    public static List<Broadcast> getAllBroadcasts() throws SQLException {
        Connection connection = getConnection();
        try {
            return BroadcastsManager.getAllBroadcasts(connection);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    public static Map<String, Broadcast> getAllBroadcastsNoEliminados(Usuario usuario) throws SQLException {
        List<BCMConexionDO> listOfConnections = BCMConexionBO.getConexiones();
        Map<String, Broadcast> broadcastsNoEliminadosHM = new HashMap<String, Broadcast>();


        for (BCMConexionDO conexionBO : listOfConnections) {
            try {
                Connection connection = getConnection(conexionBO.getId());
                logger.info("connection= " + connection + ", conexionBO.getNombre()=" + conexionBO.getNombre() + ", conexionBO.getId()=" + conexionBO.getId());
                List<Broadcast> broadcastsNoEliminados = BroadcastsManager.getAllBroadcastsNoEliminados(connection, usuario.getId());
                for (Broadcast bc : broadcastsNoEliminados) {
                    broadcastsNoEliminadosHM.put(conexionBO.getId().split("\\.")[0] + "-" + bc.getId(), bc);
                }

                //logger.info("BroadcastsManager.getAllBroadcastsNoEliminados(" + connection + ") => " + BroadcastsManager.getAllBroadcastsNoEliminados(connection).size());
                closeConnection(connection);
            } catch (SQLException sqle) {
                throw sqle;
            } finally {

            }
        }
        return broadcastsNoEliminadosHM;
    }

    /*
     * Update de un broadcast en la bd.
     */
    public static int updateBroadcast(Broadcast broadcast, Operador operador, String nombre,
                                      String descripcion, Date fechaInicio, Date fechaFin, int repetirCada, String baseControl,
                                      int baseControlTasa, int tasa, Broadcast.Estado estado, String estadoDetalle, Broadcast.BaseTipo baseTipo,
                                      String baseSQL, Conexion baseDB, Broadcast.ListaNegraTipo listaNegraTipo, String listaNegraSQL,
                                      Conexion listaNegraDb, boolean aprobado, int separacion_mensajes, String bcm,String dwTipoProducto,int dwTipoProductoId,int dwMedioId,String service_code) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return BroadcastsManager.updateBroadcast(connection, broadcast, operador, nombre, descripcion, fechaInicio, fechaFin, repetirCada, baseControl, baseControlTasa, tasa, estado, estadoDetalle, baseTipo, baseSQL, baseDB,
                    listaNegraTipo, listaNegraSQL, listaNegraDb, aprobado, separacion_mensajes,dwTipoProducto,dwTipoProductoId,dwMedioId,service_code);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    /*
     * Update de un broadcast, campo baseArchivo.
     */
    public static int updateBroadcastBaseArchivo(Broadcast broadcast, String basePathArchivo, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return BroadcastsManager.updateBroadcastBaseArchivo(connection, broadcast, basePathArchivo);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    public static int updateBroadcastListaNegraArchivo(Broadcast broadcast, String basePathArchivo, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return BroadcastsManager.updateBroadcastListaNegraArchivo(connection, broadcast, basePathArchivo);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    public static int updateBroadcastEstado(Broadcast broadcast, Broadcast.Estado estado, String estadoDetalle, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return BroadcastsManager.updateBroadcastEstado(connection, broadcast, estado, estadoDetalle);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

}
