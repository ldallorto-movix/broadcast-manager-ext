package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import com.movixla.broadcast.shared.Accion;
import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.Conexion;
import com.movixla.broadcast.shared.manager.AccionesManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andres
 * Date: 25-may-2009
 * Time: 18:02:57
 */
public class AccionesManagerWrapper extends ManagerWrapper {

	/*
	 * A�ade una accion a la base de datos.
	 */
    public static int addAccion(Broadcast broadcast, String accionSQL, Conexion accionDB, Accion.Cuando cuando, int orden, Accion.Estado estado, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return AccionesManager.addAccion(connection, broadcast, accionSQL, accionDB, cuando, orden, estado);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    /*
     * Obtiene una accion por id.
     */
    public static Accion getAccionById(int id) throws SQLException {
        Connection connection = getConnection();
        try {
            return AccionesManager.getAccionById(connection, id);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

    /*
     * Obtiene la lista de acciones configuradas para un broadcast.
     */
    public static List<Accion> getAccionesByBroadcast(Broadcast broadcast, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return AccionesManager.getAccionesByBroadcast(connection, broadcast);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

    /*
     * Actualiza las propiedades de una accion.
     */
    public static int updateAccion(Accion accion, String accionSQL, Conexion accionDB, Accion.Cuando cuando, int orden, Accion.Estado estado, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return AccionesManager.updateAccion(connection, accion, accionSQL, accionDB, cuando, orden, estado);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    /*
     * Elimina una accion de BD.
     */
    public static void deleteAccion(Accion accion, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            AccionesManager.deleteAccion(connection, accion);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }


}
