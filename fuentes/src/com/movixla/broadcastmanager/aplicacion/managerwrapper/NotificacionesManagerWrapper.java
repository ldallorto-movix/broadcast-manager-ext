package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcast.shared.Notificacion;
import com.movixla.broadcast.shared.manager.NotificacionesManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andres
 * Date: 25-may-2009
 * Time: 18:30:17
 */
public class NotificacionesManagerWrapper extends ManagerWrapper {

    private static Logger logger = Logger.getLogger(NotificacionesManagerWrapper.class);

    /*
     * A�ade una notificacion a la bd.
     */
    public static int addNotificacion(Broadcast broadcast, Notificacion.Tipo tipo, String destinatarios, int cada, Notificacion.Perfil perfil, Notificacion.Estado estado, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return NotificacionesManager.addNotificacion(connection, broadcast, tipo, destinatarios, cada, perfil, estado);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

    /*
     * Obtiene una notificacion por Id
     */
    public static Notificacion getNotificacionById(int id) throws SQLException {
        Connection connection = getConnection();
        try {
            return NotificacionesManager.getNotificacionById(connection, id);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }


    }

    /*
     * Obtiene la lista de notificaciones para un broadcast.
     */
    public static List<Notificacion> getNotificacionesByBroadcast(Broadcast broadcast, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return NotificacionesManager.getNotificacionesByBroadcast(connection, broadcast);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

    public static int updateNotficacion(Notificacion notificacion, Notificacion.Tipo tipo, String destinatarios, int cada, Notificacion.Perfil perfil, Notificacion.Estado estado, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return NotificacionesManager.updateNotficacion(connection, notificacion, tipo, destinatarios, cada, perfil, estado);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }


    public static void deleteNotificacion(Notificacion notificacion, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            NotificacionesManager.deleteNotificacion(connection, notificacion);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }

}
