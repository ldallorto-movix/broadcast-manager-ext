package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.movix.shared.Operador;
import com.movixla.broadcastmanager.aplicacion.domobj.Usuario;
import com.movixla.broadcastmanager.aplicacion.manager.UsuariosManager;

/**
 * Created by Andres
 * Date: 25-may-2009
 * Time: 18:40:05
 */
public class UsuariosManagerWrapper extends ManagerWrapper {
	
	protected static final Logger logger = Logger.getLogger(UsuariosManagerWrapper.class);
	
	/*
	 * Obtiene un usuario por nombre de usuario y contraseľa
	 */
    public static Usuario getUsuarioByUsernamePassword(String username, String password) throws SQLException {
        //DESPUES DE LA INTEGRACION EL BCM1 ES QUIEN MANEJA LOS USUARIOS
        Connection connection = getConnection("bcm5.bd");
        try {
            return UsuariosManager.getusuarioByUsernamePassword(connection, username, password);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }
    
    /*
     * Obtiene el usuario por nombre de usuario
     */
    public static Usuario getUsuarioByUsername(String username) throws SQLException {
        //DESPUES DE LA INTEGRACION EL BCM1 ES QUIEN MANEJA LOS USUARIOS
        Connection connection = getConnection("bcm1.bd");
        try {
        	logger.info ("Obteniendo registro de usuario  : " +username);
            return getusuarioByUsernameOnly(connection, username);
        } finally {
            closeConnection(connection);
        }
    }


	public static Usuario getusuarioByUsernameOnly(Connection connection, String username) throws SQLException {
				
			String sql = "SELECT * FROM usuarios WHERE username = ? ";

	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        Usuario user = null;
	        
	        logger.info("Select ejecutado para usuario : " +username);
	        
	        try {
	            ps = connection.prepareStatement(sql);
	            ps.setString(1, username);
	            rs = ps.executeQuery();

	            if (rs.next()) {
	            	logger.info("Se obtuvieron los datos del usuario: " +username+ " id : " +rs.getInt("id"));
	            	int id = rs.getInt("id");
	            	logger.info("id : " +id);
	            	String uname = rs.getString("username");
	            	logger.info ("username: " +uname);
	            	user = new Usuario(id, uname, rs.getString("nombre"), Usuario.Tipo.ADMIN,  rs.getInt("perfil_id"));	            	
	            }

	        } finally {
	            closeStatements(rs, ps);
	        }

	        logger.info("Se obtuvo el usuario de BD : " +user.getId());
	        return user;
		
	}
	
	 public static void closeStatements(ResultSet rs, PreparedStatement ps) {
	        closeResultSet(rs);
	        closePreparedStatement(ps);
	    }
	 
	 public static void closePreparedStatement(PreparedStatement ps) {
	        if (ps != null) {
	            try {
	                ps.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }

	    }

	    public static void closeResultSet(ResultSet rs) {
	        if (rs != null) {
	            try {
	                rs.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }

	    }
	    
	    
	    public static List<Operador> getOperadoresDeUsuarioId(Connection connection, int uid) throws SQLException {
	        
	        try {
	            PreparedStatement preparedStatement = connection.prepareStatement("SELECT operador_id FROM usuarios_operadores WHERE usuario_id=? ");
	            preparedStatement.setInt(1, uid);
	            preparedStatement.execute();

	            List<Operador> operadores = new ArrayList<Operador>();

	            ResultSet rs = preparedStatement.getResultSet();

	            while (rs.next()) {
	                int oid = rs.getInt("operador_id");

	                Operador operador;
	                if (oid == -1) {
	                    operador = Operador.DUMMY;
	                } else {
	                    operador = Operador.getOperadorPorIdBD(oid);
	                }

	                if (operador != null) {
	                    if (!operadores.contains(operador)) {
	                        operadores.add(operador);
	                    }
	                }
	            }

	            preparedStatement.close();
	            rs.close();

	            return operadores;
	        } finally {
	            //Tiempos.logTiempo(l);
	        }
	    }
}
