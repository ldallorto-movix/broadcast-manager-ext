package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import com.movixla.broadcast.shared.Broadcast;
import com.movixla.broadcastmanager.aplicacion.domobj.Mensaje;
import com.movixla.broadcastmanager.aplicacion.manager.MensajesManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andres
 * Date: 25-may-2009
 * Time: 18:23:17
 */
public class MensajesManagerWrapper extends ManagerWrapper {
    
	/*
	 * A�ade un mensaje a la bd.
	 */
	public static int addMensaje(Broadcast broadcast, Mensaje.Tipo tipo, String servicio, String precio, String la, String texto,String url, int orden, Mensaje.Estado estado, String bcm,String servicio_regala, String sms_final) throws SQLException {

        Connection connection = getConnection(bcm + ".bd");
        try {
            return MensajesManager.addMensaje(connection, broadcast, tipo, servicio, precio, la, texto,  url, orden, estado,servicio_regala, sms_final);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

	/*
	 * Obtiene el mensaje segun el id
	 */
    public static Mensaje getMensajeById(int id, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return MensajesManager.getMensajeById(connection, id);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

    /*
     * Obtiene el listado de mensajes del broadcast.
     */
    public static List<Mensaje> getMensajeByBroadcast0(Broadcast broadcast, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return MensajesManager.getMensajeByBroadcast0(connection, broadcast);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }
    
    public static Mensaje getMensajeByBroadcast(Broadcast broadcast, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return MensajesManager.getMensajeByBroadcast(connection, broadcast);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }

    public static int updateMensaje(Mensaje mensaje, Mensaje.Tipo tipo, String servicio, String precio, String la, String texto, String url, int orden, Mensaje.Estado estado, String bcm,String servicio_regala, String sms_final) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            return MensajesManager.updateMensaje(connection, mensaje, tipo, servicio, precio, la, texto, url, orden, estado,servicio_regala, sms_final);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }

    }


    public static void deleteMensaje(Mensaje mensaje, String bcm) throws SQLException {
        Connection connection = getConnection(bcm + ".bd");
        try {
            MensajesManager.deleteMensaje(connection, mensaje);
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            closeConnection(connection);
        }
    }


}
