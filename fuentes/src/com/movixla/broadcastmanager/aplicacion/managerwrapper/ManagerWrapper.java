package com.movixla.broadcastmanager.aplicacion.managerwrapper;

import com.movix.shared.db.DBManager;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Andres
 * Date: 25-may-2009
 * Time: 17:59:50
 */
public class ManagerWrapper {
    public static Connection getConnection() throws SQLException {
        return DBManager.getConnection();
    }

    public static Connection getConnection(String prefijo) throws SQLException {
        return DBManager.getConnection(prefijo);
    }


    public static void closeConnection(Connection conn) throws SQLException {
        DBManager.closeConnection(conn);
    }

}
