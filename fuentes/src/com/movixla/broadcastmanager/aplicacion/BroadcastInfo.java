package com.movixla.broadcastmanager.aplicacion;

/**
 * Created by Andres
 * Date: 13-jul-2009
 * Time: 12:08:11
 */
public class BroadcastInfo {
    int acciones, notificaciones, restricciones;

    /*
     * Constructor con los 3 parametros.
     */
    public BroadcastInfo(int acciones, int notificaciones, int restricciones) {
        this.acciones = acciones;
        this.notificaciones = notificaciones;
        this.restricciones = restricciones;
    }

    /*
     * Obtener las acciones
     */
    public int getAcciones() {
        return acciones;
    }

    /*
     * Obtener las notificaciones
     */
    public int getNotificaciones() {
        return notificaciones;
    }

    public int getRestricciones() {
        return restricciones;
    }


    public String toString() {
        return "BroadcastInfo{" +
                "acciones=" + acciones +
                ", notificaciones=" + notificaciones +
                ", restricciones=" + restricciones +
                '}';
    }
}
