package com.movixla.broadcastmanager.context;

import com.movix.shared.util.Util;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author Andres Cheuque
 *         Date: 29-ene-2008
 *         Time: 10:15:39
 */
public class AppContextListener implements ServletContextListener {
	
	/*
	 * Inicializando contexto de Admin.
	 */
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String appName = "Broadcast Manager";
        try {
            System.out.println(appName + " Iniciando contexto");
            System.out.println(appName + " Cargando Configuracion");
            Util.cargarConfiguracion(servletContextEvent.getServletContext(), "configuracion");


            System.out.println(appName + " Inciando Log4j");
            String log4jPropertiesFilePath = servletContextEvent.getServletContext().getRealPath("WEB-INF/log4j.properties");
            PropertyConfigurator.configure(log4jPropertiesFilePath);

            /*//System.out.println(appName + " Contenedor Broadcast");
            ClienteBroadcastManager.getInstance();

            //System.out.println(appName + " Conexiones DB");
            ConexionManager.loadConexiones(ClienteBroadcastManager.getInstance().getConexiones());*/

            System.out.println(appName + " Inciado correctamente");
        } catch (Throwable e) {
            System.out.println(appName + " ERROR AL INICIAR");
            e.printStackTrace();
        }
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}